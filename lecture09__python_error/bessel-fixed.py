from scipy.misc import factorial as fact

def term( m,x ):
    return ( ( -1 ) ** m ) / ( fact( m ) ** 2 ) * (x/2) ** (2*m)

value    = 1.0
max_term = 40
my_sum   = 0.0
for i in range( 0,max_term ):
    my_sum += term( i,value )
    print ( my_sum)

print( 'series gives %f' % my_sum )

#   The following is a reference case (calibration)
from scipy.special import j0 as bessel
print( 'scipy gives %f' % bessel( value ) )
print( 'error is %f' % ( my_sum-bessel( value ) ) )
