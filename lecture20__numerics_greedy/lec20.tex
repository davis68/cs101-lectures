%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Scientific Python}
\subtitle{CS 101}
\author{\texttt{Optimization II:  Greedy Algorithms}}
\date{\texttt{lecture20} $\cdot$ \texttt{numerics/greedy}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{framenumber}{0}
\setcounter{showProgressBar}{1}
\setcounter{showSlideNumbers}{1}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{numerics/greedy} Objectives}

\begin{enumerate}[label=\roman*]
  \item  Identify when a problem is a good candidate for a heuristic solution.
  \item  Apply a hill-climbing heuristic optimization technique to optimize a 1D problem.
\end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limits of Brute Force}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  Brute-force search of a password: %\pause
  \end{itemize}
  \begin{Verbatim}
def check_password( pwd ):
    if pwd == 'pas':
        return True
    else:
        return False

chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
for pair in itertools.product( chars, repeat=3 ):
    pair = ''.join( pair )
    if check_password( pair ):
        print( pair )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  Brute-force search of a password: %\pause
  \end{itemize}
  $$
  \begin{array}{ll}
      & 2 \times n(\textrm{alphabet}) + n(\textrm{digits}) + n(\textrm{special}) \\
    = & 2 \times 26 + 10 + \{24\textrm{--}32\} \\
    = & \{86\textrm{--}94\}
  \end{array}
  $$
  %\pause
  \emph{per letter!}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Brute-force search}

  \begin{itemize}
  \item  Assume that a password can contain characters from the alphabet (upper- and lower-case); digits; and a selection of special characters (ampersand, dash):  86 characters.  \pause
  \end{itemize}
  \begin{center}
  \begin{tabular}{rr}
    Characters & Search Space \\ \hline
    1 & $86$ \\
    2 & $86^{2} = 7\,396$ \\ \pause
    3 & $86^{3} = 636\,056$ \\
    4 & $86^{3} = 54\,700\,816$ \\
    5 & $86^{3} = 4\,704\,270\,176$ \\ \pause
    10 & $86^{10} = 2.2 \times 10^{19}$ \\
    20 & $86^{20} = 4.9 \times 10^{38}$ \\
  \end{tabular}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Brute-force search}

  \begin{itemize}
  \item  If Python can try a password attempt every $1 \times 10^{-7}$ s, how long does it take to crack a password of length $n$?
  \end{itemize}
  \begin{center}
  \begin{tabular}{rrl}
    Characters & Search Space & Time\\ \hline
    1 & 86                    & $8.6 \times 10^{-6} \,\text{s}$ \\
    2 & $7\,396$              & $7.4 \times 10^{-4} \,\text{s}$ \\
    3 & $636\,056$            & $6.4 \times 10^{-2} \,\text{s}$ \\
    4 & $54\,700\,816$        & $5.4 \,\text{s}$ \\
    5 & $4\,704\,270\,176$    & $470.4 \,\text{s}$ \\ \pause
    10 & $2.2 \times 10^{19}$ & $1.9 \times 10^{14} \,\text{s} = 6 \times 10^{6} \,\text{a}$ \\ \pause
    20 & $4.9 \times 10^{38}$ & $4.9 \times 10^{31} \,\text{s}$ \\
  \end{tabular}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Heuristic optimization}

  \begin{itemize}
  \item  In many cases, a ``good-enough'' solution is fine.  \pause
  \item  If we have a figure of \emph{relative} merit, we can classify candidate solutions by how good they are.  \pause
  \item  Heuristic algorithms don't guarantee the `best' solution, but are often adequate (and the only choice!).
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Greedy Algorithms}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Hill-climbing algorithm}

  \begin{itemize}
  \item  {\textbf Strategy}:  Always selecting neighboring candidate solution which improves on this one. \pause
  \item  {\textbf Analogy}:  Trying to find the highest hill by only taking a step uphill from where you are. \pause
  \item  {\textbf Pitfall}:  Finding a \emph{local} optimum instead of the global optimum.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Hill-climbing algorithm}

  \begin{itemize}
  \item  Set up a figure of merit $f$.
  \item  Select a starting guess $x_0$.
  \item  Change a feature of the guess.
  \item  If this improves, keep it and cycle.
  \item  If no improvement is possible, terminate.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example in 1D}

  $$
  f( x )
  =
  100 - (x-5)^2
  \hspace{1 cm}
  x \in \{ -10,+10 \},
  $$
  \includegraphics[height=0.5\textheight]{./img/lec21-1d-hill.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example in 2D}

  $$
  f( x,y )
  =
  \frac{1}{\sqrt{2x^2+2y^2}} \left(
  \cos^4 x - 2 \cos^2 x \sin^2 y + \sin^4 y
  \right)
  \hspace{1 cm}
  x \in \{ +1,+5 \},
  y \in \{ +1,+5 \}
  $$
  \includegraphics[height=0.5\textheight]{./img/lec21-random-walk-1.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example in 2D}

  $$
  f( x,y )
  =
  \frac{1}{\sqrt{2x^2+2y^2}} \left(
  \cos^4 x - 2 \cos^2 x \sin^2 y + \sin^4 y
  \right)
  \hspace{1 cm}
  x \in \{ +1,+5 \},
  y \in \{ +1,+5 \}
  $$
  \includegraphics[height=0.5\textheight]{./img/lec21-hill-climbing-2.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Setup}

  \begin{Verbatim}
import numpy as np
import matplotlib.pyplot as plt
import itertools

n = 10
items   = list( range( n ) )
weights = np.random.uniform( size=(n,) ) * 50
values  = np.random.uniform( size=(n,) ) * 100
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Setup}

  \begin{Verbatim}
def f( wts, vals ):
    total_weight = 0
    total_value = 0

    for i in range( len( wts ) ):
        total_weight += wts[ i ]
        total_value  += vals[ i ]

    if total_weight >= 50:
        return 0
    else:
        return total_value
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Tracking cases}

  \begin{Verbatim}
max_value = 0.0
max_set = None
lists = []
for i in range(n):
    for set in itertools.combinations( items,i ):
        wts  = []
        vals = []
        for item in set:
            wts.append( weights[ item ] )
            vals.append( values[ item ] )
        value = f( wts,vals )
        lists.append( ( wts, value ) )
        if value > 0:
            print( value, wts )
        if value > max_value:
            max_value = value
            max_set = set
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Tracking cases}

  \begin{Verbatim}
array = np.array( lists )
plt.plot( array[:,1], 'b.' )
plt.xlim( ( 0, len(lists) ) )
plt.show()
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Brute-force search}

  \begin{Verbatim}
import itertools

max_value = 0.0
max_set = None
for i in range(n):
    for set in itertools.combinations( items,i ):
        wts  = []
        vals = []
        for item in set:
            wts.append( weights[ item ] )
            vals.append( values[ item ] )
        value = f( wts,vals )
        if value > max_value:
            max_value = value
            max_set = set
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Hill-climbing search}

  \begin{Verbatim}
max_wt = 50.0

wts_orig  = wts[ : ]
vals_orig = vals[ : ]

best_vals = [ ]
best_wts  = [ ]
best_vals.append( max( vals ) )
best_wts.append( wts[ vals.index( max( vals ) ) ] )
wts.remove( wts[ vals.index( max( vals ) ) ] )
vals.remove( max( vals ) )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Hill-climbing search}

  \begin{Verbatim}
while sum( best_wts ) + wts[ vals.index( max( vals ) ) ] \
        < max_wt:
    best_vals.append( max( vals ) )
    best_wts.append( wts[ vals.index( max( vals ) ) ] )
    wts.remove( wts[ vals.index( max( vals ) ) ] )
    vals.remove( max( vals ) )

wts  = wts_orig[ : ]
vals = vals_orig[ : ]
  \end{Verbatim}
\end{frame}

\end{document}
