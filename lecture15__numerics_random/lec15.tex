%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Scientific Python}
\subtitle{CS 101}
\author{\texttt{Random Numbers}}
\date{\texttt{lecture15} $\cdot$ \texttt{numerics/random}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{framenumber}{0}
\setcounter{showProgressBar}{1}
\setcounter{showSlideNumbers}{1}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Randomness}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{numerics/random} Objectives}

\begin{enumerate}[label=\roman*]
  \item  Explain how random numbers are generated from deterministic algorithms.
  \item  Distinguish the three basic random distributions (uniform, normal, integer) and know when to apply them.
  \item  Plot a histogram.
\end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Randomness}

  \begin{itemize}
  \item  Consider the following sequences of numbers:
  \end{itemize}
  $$
  7\,8\,5\,3\,9\,8\,1\,6\,3\,3\,9\,7\,4\,4\,8\,3\,0\,9\,6\,1\,5\,6\,6\,0\,8\,4\,...
  $$
  \pause
  $$
  +1, -\frac{1}{3}, +\frac{1}{5}, -\frac{1}{7}, +\frac{1}{9}, -\frac{1}{11}, +\frac{1}{13}, -\frac{1}{15}, ...
  $$  \pause
  \begin{itemize}
  \item  These are derived from the same rule ($\pi$/4)---but one seems ``random'' to us.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Randomness}

  \begin{itemize}
  \item  \emph{Pseudorandom} numbers come from computer formulae. %\pause
  \item  The formula uses a \emph{seed} (often the system clock time) to start the sequence. %\pause
  \item  It then returns a new number unpredictable to you (but predictable to the formula!) each time you query the function. %\pause
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Randomness}

  \begin{itemize}
  \item  \emph{Pseudorandom} numbers come from computer formulae. %\pause
  \item  The formula uses a \emph{seed} (often the system clock time) to start the sequence. %\pause
  \item  It then returns a new number unpredictable to you (but predictable to the formula!) each time you query the function. %\pause
  \item  NumPy uses the \emph{Mersenne twister}, based on prime number distributions (but you don't need to know this). %\pause
  \item  Dozens of distributions are available---let's see a few.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Randomness}

  \begin{itemize}
  \item  There are different ways a number can be random:  we call these \emph{distributions}.
  \end{itemize}
  %\includegraphics[width=0.8\textwidth]{./img/dist-all.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Discrete distribution}

  \begin{itemize}
  \item  Select an integer within a designated range (which works the same as \texttt{range}).
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-die6.pdf}
  \begin{Verbatim}
np.random.randint( 1,7 )  # random int, [1,7)
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Discrete distribution}

  \begin{itemize}
  \item  Select an integer within a designated range (which works the same as \texttt{range}).
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-die10.pdf}
  \begin{Verbatim}
np.random.randint( 10 )  # random int, [0,10)
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Discrete distribution}

  \begin{itemize}
  \item  \texttt{randint} can return many values at a time using the \texttt{size} argument.
  \end{itemize}
  %\pause
  \begin{Verbatim}
np.random.randint( 1,7, size=5 )
np.random.randint( 0,6, size=5 ) + 1
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Uniform distribution}

  \begin{itemize}
  \item  Select any value between 0 and 1 with equal probability.
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-uniform.pdf}
  \begin{Verbatim}
np.random.uniform()
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Uniform distribution}

  \begin{itemize}
  \item  Select any value in a range with equal probability.
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-uniform-1-4.pdf}
  \begin{Verbatim}
np.random.uniform(-1,4)
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Uniform distribution}

  \begin{itemize}
  \item  \texttt{uniform} can return many values at a time using the \texttt{size} argument.
  \end{itemize}
  %\pause
  \begin{Verbatim}
np.random.uniform( size=4 )
np.random.uniform( -1,+1, size=10 )
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Normal distribution}

  \begin{itemize}
  \item  Select a random number selected from the \emph{normal} distribution with mean 0 and standard deviation 1.  (a bell curve)
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-normal.pdf}
  \begin{Verbatim}
np.random.normal()
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Normal distribution}

  \begin{itemize}
  \item  Select a random number selected from the \emph{normal} distribution with mean 1 and standard deviation 1.
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-normal+1.pdf}
  \begin{Verbatim}
np.random.normal() + 1
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Normal distribution}

  \begin{itemize}
  \item  Select a random number selected from the \emph{normal} distribution with mean 0 and standard deviation 4.
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-normals4.pdf}
  \begin{Verbatim}
np.random.normal(scale=4)
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Normal distribution}

  \begin{itemize}
  \item  Select a random number selected from the \emph{normal} distribution with mean 1 and standard deviation 4.
  \end{itemize}
  \includegraphics[width=0.6\textwidth]{./img/dist-normal+1s4.pdf}
  \begin{Verbatim}
np.random.normal(scale=4) + 1
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Normal distribution}

  \begin{itemize}
  \item  \texttt{normal} can return many values at a time using the \texttt{size} argument.
  \end{itemize}
  %\pause
  \begin{Verbatim}
np.random.normal( size=5 )
np.random.normal( size=5 ) + 10
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example}

  \begin{itemize}
  \item  Number guessing (a game for the easily entertained):
  \end{itemize}
  \begin{Verbatim}
import numpy as np
number = np.random.randint( 10 )+1
guess = input( 'Guess the number between 1 and 10:' )
while int( guess ) != number:
    guess = input( 'Nope.  Try again:' )
print( 'You did it.  Hooray.' )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{choice}}

  \begin{itemize}
  \item  \texttt{choice} randomly samples a one-dimensional array (rather, the first dimension of the array).
  \end{itemize}
  \begin{Verbatim}
x = [ 'red','yellow','blue','jale','ulfire' ]
np.random.choice(x)      # random color
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{choice}}

  \begin{itemize}
  \item  \texttt{choice} randomly samples a one-dimensional array but can do so \emph{without replacement}. %\pause
  \item  Replacement means the difference between pulling a card from a deck and putting it back before drawing again (or not). %\pause
  \end{itemize}
  \begin{Verbatim}
deck = np.array( range( 1,53 ) )
c = np.random.choice( deck, size=5, replace=False )
  \end{Verbatim}
  %\pause
  \begin{itemize}
  \item  The foregoing code draws five cards from a deck (no repeat cards allowed).
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{shuffle}}

  \begin{itemize}
  \item  \texttt{shuffle} randomly reorders an array in place.
  \end{itemize}
  \begin{Verbatim}
deck = np.array( range( 1,53 ) )
np.random.shuffle( deck )
  \end{Verbatim}
  \pause
  \begin{itemize}
  \item  The foregoing code shuffles a deck of cards.  \pause
  \item  What is the return type of \texttt{shuffle} and \texttt{choice}? %\pause
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Question}

  Which of the following will \emph{not} reproduce the behavior of a six-sided die in \texttt{c}?

  \begin{enumerate}[label=\Alph*]
  \item
  \begin{Verbatim}
c = np.random.normal( 6 ) + 1
  \end{Verbatim}
  \item
  \begin{Verbatim}
x = np.array( range( 1,7 ) )
c = np.random.choice( x )
  \end{Verbatim}
  \item
  \begin{Verbatim}
c = np.random.randint( 6 )+1
  \end{Verbatim}
  \item
  \begin{Verbatim}
d = np.random.uniform() * 6
c = int(d) + 1
  \end{Verbatim}
  \end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Question}

  Which of the following will \emph{not} reproduce the behavior of a six-sided die in \texttt{c}?

  \begin{enumerate}[label=\Alph*]
  \item
  \begin{Verbatim}[commandchars=\\\{\},commentchar=\%]
c = np.random.normal( 6 ) + 1    \correctstar
  \end{Verbatim}
  \item
  \begin{Verbatim}
x = np.array( range( 1,7 ) )
c = np.random.choice( x )
  \end{Verbatim}
  \item
  \begin{Verbatim}
c = np.random.randint( 6 )+1
  \end{Verbatim}
  \item
  \begin{Verbatim}
d = np.random.uniform() * 6
c = int(d) + 1
  \end{Verbatim}
  \end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Histograms}

  \begin{itemize}
  \item  Histograms plot the number of times a value occurs in a data set.
  \item  With enough values, the histogram approaches the ``true'' distribution.
  \end{itemize}
  %\pause
  \begin{columns}
    \begin{column}{0.48\textwidth}
    \includegraphics[width=\textwidth]{./img/hist-normal.pdf}
    \end{column}
    \begin{column}{0.48\textwidth}
    \includegraphics[width=\textwidth]{./img/dist-normal.pdf}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Histograms}

  \begin{itemize}
  \item  \texttt{hist} (MatPlotLib) creates a \emph{histogram}.
  \end{itemize}
  %\pause
  \begin{Verbatim}
x = np.random.randint(0,100,size=10000)
plt.hist(x)
plt.show()
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Histograms}

  \includegraphics[width=0.6\textwidth]{./img/num_random__carnuf_hist_diagram.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Randomness}

  \begin{itemize}
  \item  How evenly are the digits of $\pi$ distributed?
  \end{itemize}
  \begin{Verbatim}
pi = open( 'pi.txt','r' ).read().strip().replace( '.','' )

digits = []
for i in range( 10 ):
    digits.append( pi.count( str( i ) ) )

import matplotlib.pyplot as plt
plt.bar( range(0,len(digits)),digits )
plt.xticks( range(0,len(digits)) )
plt.show()
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Conclusion}

  \begin{itemize}
  \item  How to access \texttt{np.random}
  \item  \texttt{np.random.uniform}
  \item  \texttt{np.random.normal}
  \item  \texttt{np.random.randint}
  \item  \texttt{np.random.shuffle}
  \item  \texttt{np.random.choice}
  \item  How to plot and interpret histograms
  \item  How to identify each distribution
  \end{itemize}
\end{frame}

\end{document}
