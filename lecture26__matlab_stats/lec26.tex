%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Modeling in MATLAB}
\subtitle{CS 101}
\author{\texttt{Statistics}}
\date{\texttt{lecture26} $\cdot$ \texttt{matlab/stats}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{matlab/stats} Objectives}

  \begin{enumerate}[label=\roman*]
  \item  Calculate basic statistics of arrays using MATLAB.
  \item  Generate random numbers in arrays.
  \item  Use interpolation to find function values.
  \item  Use left-division to solve matrix equations efficiently.
  \item  Time code using \texttt{tic} and \texttt{toc}.
  \end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Statistics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Statistical quantities}

  \begin{itemize}
  \item  Many operations are available:
    \begin{itemize}
    \item  \texttt{mean}, \texttt{median}, \texttt{std}
    \item  \texttt{max}, \texttt{min}, \texttt{range}
    \item  \texttt{iqr}, \texttt{corrcoef} (the correlation coefficient of two random variables is a measure of their linear dependence) \textcolor{CS101Blue}{(don't worry about these)}
    \item  \texttt{sort}, \texttt{sum}, \texttt{cumsum}, \texttt{prod}, \texttt{cumprod}
    \item  \texttt{boxplot}, \texttt{hist}
    \end{itemize}
  \end{itemize}
  \begin{Verbatim}
x = randn( 6,1 );
y = randn( 6,1 );
A = [x y 2*y+3 ];
R = corrcoef( A );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Statistical quantities}

  \begin{Verbatim}
x = [ 1 2 3 4 5 ];
A = [ -5 0 10 ; -4 1 9 ; -3 2 8 ; -2 3 7 ; -1 4 6 ]

sort( x )
sort( x,'descend' )
sort( A )
sortrows( A )
sortrows( A,3 )
cumsum( x )

y = rand( 1000,1 );
boxplot( y )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  Brexit polling}

  \begin{Verbatim}
poll = importdata('brexit.csv');
plot( poll.data(:,2) );
plot( poll.data(:,3) );
% oh no! our plotted data disappeared!
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  Brexit polling}

  \begin{Verbatim}
poll = importdata('brexit.csv');
hold on;  % make plots persistent until closed
plot( poll.data(:,2) );
plot( poll.data(:,3) );
plot( poll.data(:,4) );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  Brexit polling}

  \begin{Verbatim}
n = numel(poll.data(:,2));

mean_r = mean( poll.data(:,2) ) * ones( n+1,1 );
stdev_r = std( poll.data(:,2) );
std_rp = mean_r+stdev_r;
std_rm = mean_r-stdev_r;
hold on
plot( poll.data(:,2), 'ro' );
plot( 0:n,mean_r, 'r-' );
plot( 0:n,std_rp, 'r--' );
plot( 0:n,std_rm, 'r--' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  Brexit polling}

  \begin{Verbatim}
n = numel(poll.data(:,2));
mean_r = rolling_mean( poll.data(:,2)', 25 );
stdev_r = rolling_std( poll.data(:,2)', 25 );
std_rp = mean_r+stdev_r;
std_rm = mean_r-stdev_r;
hold on
plot( poll.data(:,2), 'ro' );
plot( 0:n-1,mean_r, 'r-' );
plot( 0:n-1,std_rp, 'r--' );
plot( 0:n-1,std_rm, 'r--' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  Hydropower statistics}

  \begin{Verbatim}
dams = importdata( 'hydropower.csv' );
disp( dams );
plot( dams.data(:,6),dams.data(:,5),'bo' );
xlim( [ 1900 2000 ] )
  \end{Verbatim}

  \begin{itemize}
  \item  What statistics should we try to see here?
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Random Numbers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Random numbers}

  \begin{itemize}
  \item  MATLAB supports many varieties of RNG:
    \begin{itemize}
    \item  \texttt{rand}, uniform distribution $[0,1)$
    \item  \texttt{randn}, normal distribution
    \item  \texttt{randi}, random integers $[0,n)$
    \end{itemize}
  \item  Note that the interfaces for these are quite different from Python!
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{rand}}

  \begin{Verbatim}
rand( 5 );      % generate 5x5 matrix
rand( 5,1 );    % generate 5x1 column vector
10 * rand( 3 ); % 3x3 matrix from [0,10)
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{randi}}

  \begin{Verbatim}
randi( 5 );      % generate number from [1,5]
randi( 5,2 );    % generate 2x2 matrix
randi( [ -5,5 ],10,1 ); % from [-5,5] in 10x1
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{randn}}

  \begin{Verbatim}
randn();         % single normal number
randn( 5 );      % generate 5x5 matrix
randn( 5,2 );    % generate 5x2 matrix
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  Seed}

  \begin{Verbatim}
randn( 'seed',1 );
x = linspace( 0,2*pi,101 )';
y = sin( x/50 ) ./ x + .002 * randn( 101,1 );
clf
plot( x,y,'.' );
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Interpolation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Interpolation}

  \begin{itemize}
  \item  Generally, means drawing a line between data values to approximate data at other points.
  \item  ``Inter'' means between---``polare'' to polish or facet.  Distinguish interpolation from other kinds of estimation!
  \end{itemize}
  \begin{Verbatim}
x = linspace( 0,1,11 );
y = x .^ 2;
plot( x,y,'ro-' );

x_est = 0.15;
y_est = interp1( x,y,x_est );

hold on
plot( x,y,'ro-' )
plot( x_est,y_est,'bo' )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Interpolation}

  \begin{itemize}
  \item  This works well if points are close, but can have problems:
  \end{itemize}
  \begin{Verbatim}
x = linspace( 0,4*pi,11 );
y = cos( x );

x_real = linspace( 0,4*pi,501 );
y_real = cos( x_real );

x_est = pi;
y_est = interp1( x,y,x_est );

hold on
plot( x,y,'ro-' )
plot( x_est,y_est,'bo' )
plot( x_real,y_real,'g-' )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Interpolation 1/3}

  \begin{itemize}
  \item  Other options include \texttt{'nearest'} and \texttt{'cubic'}:
  \end{itemize}
  \begin{Verbatim}
x = linspace( 0,4*pi,11 );
y = cos( x );

x_real = linspace( 0,4*pi,501 );
y_real = cos( x_real );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Interpolation 2/3}

  \begin{Verbatim}
x_est_linear = 5.5;
y_est_linear = interp1( x,y,x_est_linear );

x_est_nearest = 5.5;
y_est_nearest = interp1( x,y,x_est_nearest,'nearest' );

x_est_cubic = 5.5;
y_est_cubic = interp1( x,y,x_est_cubic,'cubic' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Interpolation 3/3}

  \begin{Verbatim}
hold on
plot( x,y,'ro-' )
plot( x_est_linear,y_est_linear,'bo' )
plot( x_est_nearest,y_est_nearest,'bx' )
plot( x_est_cubic,y_est_cubic,'bd' )
plot( x_real,y_real,'g-' )
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Matrix Equations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  $$
\underline{\underline{A}}
\underline{x}
=
\underline{f}
  $$

  \begin{itemize}
  \item  This is the canonical equation of engineering.  \pause
  \item  No matter what your source equation, when it comes time to solve a problem numerically this is the equation you will use.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  $$
\underline{\underline{A}}
\underline{x}
=
\underline{f}
  $$

  \begin{itemize}
  \item  Formally, the solution is:
  \end{itemize}

  $$
\underline{\underline{A}}^{-1}
\underline{\underline{A}}
\underline{x}
=
\underline{\underline{A}}^{-1}
\underline{f}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  $$
\underline{\underline{A}}^{-1}
\underline{\underline{A}}
\underline{x}
=
\underline{\underline{A}}^{-1}
\underline{f}
  $$
  $$
\underline{\underline{I}}
\underline{x}
=
\underline{\underline{A}}^{-1}
\underline{f}
  $$
  $$
\underline{x}
=
\underline{\underline{A}}^{-1}
\underline{f}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  \begin{Verbatim}
A = [ 2 -1 0 ; -1 2 -1 ; 0 -1 2 ];
f = [ 1 2 3 ]';
x = inv( A ) * f;
  \end{Verbatim}

  $$
\underline{x}
=
\underline{\underline{A}}^{-1}
\underline{f}
  $$
  $$
\underline{x}
=
\left(
\begin{array}{ccc}
2 & -1 & 0 \\
-1 & 2 & -1 \\
0 & -1 & 2
\end{array}
\right)^{-1}
\left(
\begin{array}{ccc}
1 \\
2 \\
3
\end{array}
\right)
  $$
  \pause
  $$
\underline{x}
=
\left(
\begin{array}{ccc}
3/4 & 1/2 & 1/4 \\
1/2 & 1 & 1/2 \\
1/4 & 1/2 & 3/4
\end{array}
\right)
\left(
\begin{array}{ccc}
1 \\
2 \\
3
\end{array}
\right)
\pause
=
\left(
\begin{array}{ccc}
2.5 \\
4 \\
3.5
\end{array}
\right)
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  $$
  \left(
  \begin{array}{ccccc}
  4 & -1 & 0 & 0 & 0 \\
  -1 & 4 & -1 & 0 & 0 \\
  0 & -1 & 4 & -1 & 0 \\
  0 & 0 & -1 & 4 & -1 \\
  0 & 0 & 0 & -1 & 4 \\
  \end{array}
  \right)
  $$

  \begin{itemize}
  \item  Most engineering equations have most nonzero values near the diagonal.
  \item  This means most of the matrix is zero, and efficient to store and calculate with.
  \end{itemize}
\end{frame}
%A = [ 4 -1 0 0 0 ; -1 4 -1 0 0 ; 0 -1 4 -1 0 ; 0 0 -1 4 -1 ; 0 0 0 -1 4 ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  $$
  \left(
  \begin{array}{ccccc}
  0.268 & 0.072 & 0.019 & 0.005 & 0.001 \\
  0.072 & 0.287 & 0.077 & 0.021 & 0.005 \\
  0.019 & 0.077 & 0.288 & 0.077 & 0.019 \\
  0.005 & 0.021 & 0.077 & 0.287 & 0.072 \\
  0.001 & 0.005 & 0.019 & 0.072 & 0.268 \\
  \end{array}
  \right)
  $$

  \begin{itemize}
  \item  The inverse of a matrix does not have the same properties!
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equations}

  \begin{itemize}
  \item  MATLAB's solution:  left-division uses more sophisticated techniques:
  \end{itemize}
  \begin{Verbatim}
A = [  4 -1 0 0 0 ; ...
      -1 4 -1 0 0 ; ...
      0 -1 4 -1 0 ; ...
      0 0 -1 4 -1 ; ...
      0 0 0 -1 4 ];
f = [ 1 2 3 4 5 ]';
x = A \ f;
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Timing Code}

We can compare solution speed with \texttt{tic} and \texttt{toc}:

  \begin{Verbatim}
A = [ 1 -2 0 0 0 ; -2 1 -2 0 0 ; ...
      0 -2 1 -2 0 ; 0 0 -2 1 -2 ; ...
      0 0 0 -2 1 ];
b = [ 1 2 3 4 5 ]';

tic; x1 = inv(A) * b; toc
tic; x2 = A \ b; toc
  \end{Verbatim}
\end{frame}

\end{document}
