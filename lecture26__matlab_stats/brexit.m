poll = importdata('brexit.csv');
hold on
plot( poll.data(:,2) );
plot( poll.data(:,3) );
plot( poll.data(:,4) );

n = numel(poll.data(:,2));
mean_r = mean( poll.data(:,2) ) * ones( n+1,1 );
stdev_r = std( poll.data(:,2) );
std_rp = mean_r+stdev_r;
std_rm = mean_r-stdev_r;
hold on
plot( poll.data(:,2), 'ro' );
plot( 0:n,mean_r, 'r-' );
plot( 0:n,std_rp, 'r--' );
plot( 0:n,std_rm, 'r--' );

n = numel(poll.data(:,2));
mean_r = rolling_mean( poll.data(:,2)', 25 );
stdev_r = rolling_std( poll.data(:,2)', 25 );
std_rp = mean_r+stdev_r;
std_rm = mean_r-stdev_r;
hold on
plot( poll.data(:,2), 'ro' );
plot( 0:n-1,mean_r, 'r-' );
plot( 0:n-1,std_rp, 'r--' );
plot( 0:n-1,std_rm, 'r--' );