n = 10001
num_steps = 500000   # total iterations
heat = 0.01
starting_guess = -20
frame_mult = 100

import numpy as np
def f( x ):
    return np.sin( x ) + 0.02 * x ** 2
X = np.linspace( -50,50,n ) # set up a grid in x
Y = f( X )

# Random walk algorithm begins here. ###############################
X = np.linspace( -50,50,n ) # set up a grid in x
x = starting_guess
l = -1000/(n-1)    # step size left
r = +1000/(n-1)    # step size right

best_x = x
best_f = f( best_x )
steps = np.zeros( ( num_steps+1,2 ) )
steps[ 0,: ] = np.array( ( x,f( x ) ) )
for i in range( num_steps+1 ):
    # Take a random step, 50% chance in each direction.
    trial_x = x
    chance = np.random.uniform()
    if chance < 0.5:
        trial_x = x+l
    else:
        trial_x = x+r

    if f( trial_x ) < best_f:
        # If the solution improves, accept it.
        best_f = f( trial_x )
        best_x = trial_x
        x = trial_x
    else:
        # If the solution does not improve, sometimes accept it.
        chance = np.random.uniform()
        if chance < heat:
            x = trial_x

    # Log steps for plotting later.
    steps[ i,: ] = np.array( ( x,f( x ) ) )

####################################################################
# Plot random walk as an animation in time.
import matplotlib.pyplot as plt
fig,ax = plt.subplots()

ln, = plt.plot( [],[],'bx' )

def init():
    ax.set_xlim( -50,50 )
    ax.set_ylim( -10,50 )
    ax.plot( X,Y,'r-' )
    ax.plot( best_x,best_f,'bo' )
    return ln,

def update( frame ):
    f = frame * frame_mult
    if f > steps.shape[ 0 ]: return ln,
    ln.set_data( steps[ f,0 ],steps[ f,1 ] )
    print(f)
    return ln,

from matplotlib.animation import FuncAnimation
ani = FuncAnimation( fig,update,frames=range(num_steps//frame_mult),
                     init_func=init,blit=True,repeat=False,
                     interval=5,save_count=num_steps//frame_mult )

plt.show()

ani.save( 'rwalk.mp4' )
print('ready')