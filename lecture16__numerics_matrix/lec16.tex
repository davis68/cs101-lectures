%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Scientific Python}
\subtitle{CS 101}
\author{\texttt{Matrices}}
\date{\texttt{lecture16} $\cdot$ \texttt{numerics/matrix}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{framenumber}{0}
\setcounter{showProgressBar}{1}
\setcounter{showSlideNumbers}{1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{numerics/matrix} Objectives}

\begin{enumerate}[label=\roman*]
  \item  Use NumPy arrays to store and operate on multidimensional data.
  \item  Solve basic systems of equations using matrix–vector multiplication.
\end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{2D Arrays:  Matrices}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrices}

  \begin{Verbatim}
x = np.array( [ [ 1,2 ], [ 3,4 ] ] )
np.zeros( ( 3,3 ) )
np.ones( ( 4,4 ) )
np.eye( 4 )
x.shape
x.dtype
x = np.array( [ [ 1,2 ], [ 3,4 ] ],dtype=np.float64 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Indexing matrices}

  \includegraphics[width=0.67\textwidth]{./img/ndarray.png}

  \begin{itemize}
  \item  \texttt{numpy} indexes by \texttt{array[row][col]} or \texttt{array[ row,col ]}.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{numpy}}

  \begin{Verbatim}
x[ :,1 ]
x[ 1,: ]
x.T
x.tolist()
x.sort()
x.argsort()
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{numpy}}

  Consider a data set containing patient inflammation records for 60 patients over a period of 40 days, contained in \texttt{inflammation.csv}.

  \begin{Verbatim}
import numpy as np
data = np.loadtxt( 'inflammation.csv',delimiter=',' )
print( data.shape )

import matplotlib.pyplot as plt
plt.imshow( data )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Linear Algebra}

$$
\begin{alignat*}{7}
 x &&\; + \;&& 3y &&\; - \;&& 2z &&\; = \;&& 5 & \\
3x &&\; + \;&& 5y &&\; + \;&& 6z &&\; = \;&& 7 & \\
2x &&\; + \;&& 4y &&\; + \;&& 3z &&\; = \;&& 8 &
\end{alignat*}
$$
\pause
$$
\begin{pmatrix}
1 & 3 & -2 \\
3 & 5 & 6 \\
2 & 4 & 3 \\
\end{pmatrix}
\begin{pmatrix}
x \\
y \\
z \\
\end{pmatrix}
=
\begin{pmatrix}
5 \\
7 \\
8 \\
\end{pmatrix}
\textrm{.}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix–Matrix Multiplication}

$$
\begin{pmatrix}
a_{11} & a_{12} \\
a_{21} & a_{22} \\
\end{pmatrix}
\begin{pmatrix}
b_{11} & b_{12} \\
b_{21} & b_{22} \\
\end{pmatrix}
=
\begin{pmatrix}
a_{11}b_{11} + a_{12}b_{21} & a_{11}b_{12} + a_{12}b_{22} \\
a_{21}b_{11} + a_{22}b_{21} & a_{21}b_{12} + a_{22}b_{22} \\
\end{pmatrix}
$$
\pause
$$
\begin{pmatrix}
4 & 5 \\
3 & 6 \\
\end{pmatrix}
\begin{pmatrix}
2 & 1 \\
7 & 0 \\
\end{pmatrix}
=
\begin{pmatrix}
4\cdot 2 + 7\cdot 5 & 4\cdot 1 + 5\cdot 0 \\
3\cdot 2 + 6\cdot 7 & 3\cdot 1 + 6\cdot 0 \\
\end{pmatrix}
=
\begin{pmatrix}
43 & 4 \\
48 & 3 \\
\end{pmatrix}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix–Vector Multiplication}

$$
\begin{alignat*}{7}
3x &&\; + \;&& 2y             &&\; - \;&& z  &&\; = \;&& 1 & \\
2x &&\; - \;&& 2y             &&\; + \;&& 4z &&\; = \;&& -2 & \\
-x &&\; + \;&& \tfrac{1}{2} y &&\; - \;&& z  &&\; = \;&& 0 &
\end{alignat*}
$$

$$
\begin{pmatrix}
3 & 2 & -1 \\
2 & -2 & 4 \\
-1 & \frac{1}{2} & -1 \\
\end{pmatrix}
\begin{pmatrix}
x \\
y \\
z \\
\end{pmatrix}
=
\begin{pmatrix}
1 \\
-2 \\
0 \\
\end{pmatrix}
$$

$$
\underline{x}
=
\begin{pmatrix}
1 \\
-2 \\
-2 \\
\end{pmatrix}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Elementwise Multiplication}

$$
\begin{pmatrix}
a_{11} & a_{12} \\
a_{21} & a_{22} \\
\end{pmatrix}
*
\begin{pmatrix}
b_{11} & b_{12} \\
b_{21} & b_{22} \\
\end{pmatrix}
=
\begin{pmatrix}
a_{11}b_{11} & a_{12}b_{12} \\
a_{21}b_{21} & a_{22}b_{22} \\
\end{pmatrix}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equation}

$$
\underline{\underline{A}} \underline{x}
=
\underline{b}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equation (Wrong!)}

$$
\underline{x}
=
\frac{\underline{b}}{\underline{\underline{A}}}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Inversion}

$$
\underline{\underline{A}}^{-1}
\underline{\underline{A}} \underline{x}
=
\underline{\underline{A}}^{-1}
\underline{b}
$$
\pause
$$
\underline{\underline{I}} \underline{x}
=
\underline{\underline{A}}^{-1}
\underline{b}
$$
\pause
$$
\underline{x}
=
\underline{\underline{A}}^{-1}
\underline{b}
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix Equation Solution}

$$
\begin{alignat*}{7}
3x &&\; + \;&& 2y             &&\; - \;&& z  &&\; = \;&& 1 & \\
2x &&\; - \;&& 2y             &&\; + \;&& 4z &&\; = \;&& -2 & \\
-x &&\; + \;&& \tfrac{1}{2} y &&\; - \;&& z  &&\; = \;&& 0 &
\end{alignat*}
$$

$$
\begin{pmatrix}
3 & 2 & -1 \\
2 & -2 & 4 \\
-1 & \frac{1}{2} & -1 \\
\end{pmatrix}
\begin{pmatrix}
x \\
y \\
z \\
\end{pmatrix}
=
\begin{pmatrix}
1 \\
-2 \\
0 \\
\end{pmatrix}
$$

$$
\underline{x}
=
\begin{pmatrix}
1 \\
-2 \\
-2 \\
\end{pmatrix}
$$
\end{frame}

\end{document}
