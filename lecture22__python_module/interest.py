def present_amount(A0,p,n):
    '''
    Calculate money after compounding
    A0 for n days at p percent interest.
    '''
    return A0*(1+p/(360*100))**n

def test_present_amount():
    A0,p,n = 2.0,5,730
    A_expected = 2.213398
    A_computed = present_amount(A0,p,n)
    from math import isclose
    assert isclose(A_expected,A_computed,rel_tol=1e-6)

if __name__ == '__main__':
    print('Running tests...')
    test_present_amount()
