import numpy as np

meter  = ( 1.0,np.array( (1,0,0) ) )
second = ( 1.0,np.array( (0,1,0) ) )
kelvin = ( 1.0,np.array( (0,0,1) ) )

sq_meter  = ( 1.0,np.array( (2,0,0) ) )
g_gravity = ( 9.8,np.array( (1,-2,0) ) )

def unit_add( unit1,unit2 ):
    if not ( unit1[ 1 ] == unit2[ 1 ] ).all():
        # can't add units that don't match
        raise Exception( 'Units do not match.' )
    return ( unit1[ 0 ]+unit2[ 0 ],unit1[ 1 ] )

def unit_mult( val1,val2 ):
    if isinstance( val1,tuple ):
        # multiply units together
        return ( val1[ 0 ]*val2[ 0 ],val1[ 1 ]+val2[ 1 ] )
    elif isinstance( val1,float ):
        # multiply scalar times unit
        return ( val1*val2[ 0 ],val2[ 1 ] )
    else:
        # don't know how to multiply
        raise Exception( 'Ambiguous multiplication.' )

def unit_div( val1,val2 ):
    if isinstance( val1,tuple ) and isinstance( val2,tuple ):
        # divide units together
        return ( val1[ 0 ]/val2[ 0 ],val1[ 1 ]-val2[ 1 ] )
    elif isinstance( val1,tuple ) and isinstance( val2,float ):
        # divide unit by scalar
        return ( val1[ 0 ]/val2,val1[ 1 ] )
    elif isinstance( val1,float ) and isinstance( val2,tuple ):
        # divide scalar by unit
        return ( val1/val2[ 0 ],-val2[ 1 ] )
    else:
        # don't know how to divide
        raise Exception( 'Ambiguous division.' )

foot = unit_mult( 0.3048,meter )
sq_foot = unit_mult( foot,foot )


def main():
    y = unit_mult( 5.0,meter )
    v = unit_mult( 0.0,unit_div( meter,second ) )
    g = unit_mult( -1.0,g_gravity )
    dt = unit_mult( 0.01,second )
    for i in range( 101 ):
        v = unit_add( v,unit_mult( dt,g ) )
        y = unit_add( y,unit_mult( dt,v ) )
        print( y[ 0 ] )

if __name__ == '__main__':
    main()






