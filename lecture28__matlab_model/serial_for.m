% serial
tic
n = 200;
A = 500;
a = zeros( n,n );
for i = 1:n
      a( i ) = max( abs( eig( rand( A ) ) ) );
end  %for
toc