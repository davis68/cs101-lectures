x_samples = linspace( 0,1,11 );
y_samples = x_samples + 0.1 * randn( size( x_samples ) );

figure;
hold on;
plot( x_samples,y_samples,'ko','MarkerFaceColor','k' );

x_span = linspace( 0,1,111 );
y_interp = interp1( x_samples,y_samples,x_span );
plot( x_span,y_interp,'b--' )

coefs = polyfit( x_samples,y_samples,1 );
y_fit = polyval(coefs,x_span);
plot( x_span,y_fit,'r:' )

coefs = polyfit( x_samples,y_samples,2 );            
y_fit = polyval(coefs,x_span);            
plot( x_span,y_fit,'g-' )            

coefs = polyfit( x_samples,y_samples,3 );            
y_fit = polyval(coefs,x_span);            
plot( x_span,y_fit,'c-' )            



