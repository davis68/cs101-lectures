% This script is written and read by pdetool and should NOT be edited.
% There are two recommended alternatives:
 % 1) Export the required variables from pdetool and create a MATLAB script
 %    to perform operations on these.
 % 2) Define the problem completely using a MATLAB script. See
 %    http://www.mathworks.com/help/pde/examples/index.html for examples
 %    of this approach.
function pdemodel
[pde_fig,ax]=pdeinit;
pdetool('appl_cb',1);
set(ax,'DataAspectRatio',[1 1 1]);
set(ax,'PlotBoxAspectRatio',[1.5 1 1]);
set(ax,'XLim',[-1.5 1.5]);
set(ax,'YLim',[-1 1]);
set(ax,'XTickMode','auto');
set(ax,'YTickMode','auto');

% Geometry description:
pderect([-0.99506578947368418 1.0049342105263159 0.61348684210526283 -0.59703947368421084],'R1');
pdeellip(0.49835526315789469,0.0082236842105261054,0.2565789473684208,0.25328947368421062,...
0,'E1');
set(findobj(get(pde_fig,'Children'),'Tag','PDEEval'),'String','R1-E1')

% Boundary conditions:
pdetool('changemode',0)
pdesetbd(8,...
'dir',...
1,...
'1',...
'400')
pdesetbd(7,...
'dir',...
1,...
'1',...
'400')
pdesetbd(6,...
'dir',...
1,...
'1',...
'400')
pdesetbd(5,...
'dir',...
1,...
'1',...
'400')
pdesetbd(4,...
'dir',...
1,...
'1',...
'400')
pdesetbd(3,...
'neu',...
1,...
'1',...
'0')
pdesetbd(2,...
'neu',...
1,...
'1',...
'0')
pdesetbd(1,...
'neu',...
1,...
'1',...
'0')

% Mesh generation:
setappdata(pde_fig,'Hgrad',1.3);
setappdata(pde_fig,'refinemethod','regular');
setappdata(pde_fig,'jiggle',char('on','mean',''));
setappdata(pde_fig,'MesherVersion','preR2013a');
pdetool('initmesh')
pdetool('refine')

% PDE coefficients:
pdeseteq(1,...
'1.0',...
'0.0',...
'0',...
'1.0',...
'0:10',...
'0.0',...
'0.0',...
'[0 100]')
setappdata(pde_fig,'currparam',...
['1.0';...
'0.0';...
'0  ';...
'1.0'])

% Solve parameters:
setappdata(pde_fig,'solveparam',...
char('0','1116','10','pdeadworst',...
'0.5','longest','0','1E-4','','fixed','Inf'))

% Plotflags and user data strings:
setappdata(pde_fig,'plotflags',[2 1 1 1 1 1 6 1 0 0 0 1 1 0 0 1 0 1]);
setappdata(pde_fig,'colstring','');
setappdata(pde_fig,'arrowstring','');
setappdata(pde_fig,'deformstring','');
setappdata(pde_fig,'heightstring','');

% Solve PDE:
pdetool('solve')
