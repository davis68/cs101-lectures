%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Modeling in MATLAB}
\subtitle{CS 101}
\author{\texttt{Modeling \& Solving Equations}}
\date{\texttt{lecture28} $\cdot$ \texttt{matlab/model}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{matlab/model} Objectives}

  \begin{enumerate}[label=\roman*]
  \item  Distinguish functions from function handles.
  \item  Plot functions using \texttt{fplot}.
  \item  Minimize functions within bounds using \texttt{fminbnd} and find zeroes with \texttt{fzero}.
  \item  Apply an ODE solver, \texttt{ode45}, to produce a basic physics model.
  \item  Employ the PDE Toolbox to solve common differential equations.
  \item  Articulate a vision of subsequent programming work as a scientist or engineer.
  \end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Function Handles}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Function Handles}

  Function handles let us refer to functions without calling them (sometimes, without naming them).

  \begin{Verbatim}
function y = f( x )
  y = x .^ 2;
end %function

@f
  \end{Verbatim}
  \pause

  \begin{Verbatim}
@(x) x .^ 2
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Function Handles}

  Function handles let us refer to functions without calling them (sometimes, without naming them).

  \begin{Verbatim}
fplot(@(x) cos(x))
fplot(@(t) cos(3*t),@(t) sin(2*t))
fcontour( @(x,y) sin(x) * cos(y) )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Plotting Functions}

  \begin{itemize}
  \item  \texttt{fplot}
  \item  \texttt{plot3}
  \item  \texttt{fcontour}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Aside on functions}

  \begin{itemize}
  \item  You can define a single-line function locally using the syntax:
  \end{itemize}
  \begin{Verbatim}
f = @(t) cos( 3*t );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Plotting}

  \begin{Verbatim}
x = @(t) cos( 3*t );
y = @(t) sin( 2*t );
fplot( x,y )

t = 0:pi/50:10*pi;
st = sin(t);
ct = cos(t);
plot3(st,ct,t)
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Plotting}

  \begin{Verbatim}
f = @(x,y) sin( x ) + cos( y );
fcontour( f )

subplot(2,1,1);
x = linspace(0,10);
y1 = sin(x);
plot(x,y1)

subplot(2,1,2);
y2 = sin(5*x);
plot(x,y2)
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Optimization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Function Minimization}

  Given a function $f({x})$, find ${x} = {x}^*$ such that $f({x}^*)$ is maximized (or minimized).

  \begin{Verbatim}
x = -1:.01:2;
y = humps( x );

figure
plot( x,y )
xlabel( 'x' )
ylabel( 'f(x)' )
grid on

xstar = fminbnd( @humps,0.3,1 )
[ xstar,ystar ] = fminbnd( @humps,0.3,1 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Function Zeroes}

  Given a function $f({x})$, find ${x} = {x}^*$ such that $f({x}^*) = 0$.

  \begin{Verbatim}
function [ y ] = f( x )
  y = cos( x ) - exp( -x ) + 4;
end %function

xstar = fzero( @f,10 );
  \end{Verbatim}
  \pause

  \begin{Verbatim}
xstar = fzero( @(x) cos( exp( x ) ) - 1,0 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Finding Solutions}

  $$
\cos x = e^{-x} - 4
  $$

  \pause
  \begin{itemize}
  \item  Rewrite this equation as a function in standard form:
  \end{itemize}

  $$
f(x)
=
\cos x - e^{-x} + 4
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Finding Solutions}

  $$
f(x)
=
\cos x - e^{-x} + 4
  $$

  \begin{itemize}
  \item  Solve the equation with \texttt{fzero} (if seeking zeros) or \texttt{fminbnd} (if seeking minima).  \pause
  \item  \texttt{x = fzero( @f,x0 )} tries to find a zero of fun near \texttt{x0}.
  \item  \texttt{x = fminbnd( @f,x1,x2 )} returns a value \texttt{x} that is a local minimizer of the function that is described in fun in the interval $[\texttt{x1}\,\texttt{x2}]$.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{Verbatim}
x = -1:.01:2;
y = humps( x );   % a built-in ``interesting'' function

figure
plot( x,y )
xlabel( 'x' )
ylabel( 'f(x)' )
grid on

xstar = fminbnd( @humps,0.3,1 )
[ xstar,ystar ] = fminbnd( @humps,0.3,1 )
  \end{Verbatim}
  \pause
  \begin{itemize}
  \item  Note the \texttt{@} on the function name!  \pause
  \item  How do we maximize a function?
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Ordinary Differential Equations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{ODEs}

  $$
\frac{dx}{dt}
=
-9.8 t
  $$

  $$
x(t)
=
\int_0^t d\tau \, \left(-9.8 \tau\right)
=
-9.8 \frac{t^{2}}{2}
  $$
  \pause

  \begin{Verbatim}
[ t x ] = ode45( @(t,x) -9.8.*t,[ 0 10 ],0 );
plot( t,x )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{ODEs}

  $$
\frac{dx}{dt}
=
x t
  $$
  
  $$
x_{0} = 1
  $$

  $$
x(t)
=
\int_0^t d\tau \, x \tau
=
x \frac{t^{2}}{2}
  $$
  \pause

  \begin{Verbatim}
[ t x ] = ode45( @(t,x) x.*t,[ 0 10 ],0 );
plot( t,x )
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{ODEs}

  $$
\frac{dx}{dt}
=
\sin t
  $$

  $$
x(t)
=
\int_0^t d\tau \, \sin \tau
=
-\cos t + 1
  $$
  \pause

  \begin{Verbatim}
[ t x ] = ode45( @(t,x) cos( t ),[ 0 10 ],0 );
plot( t,x )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{ODEs}

  $$
\frac{dy}{dx} + x
=
0
  $$
  $$
x \in [0,1],\,
y_0 = 1
  $$
  \pause

  \begin{Verbatim}
xspan = [ 0 1 ];
y0 = 1;
[ x y ] = ode45( @(t,x) -x,xspan,y0 );
plot( x,y,'r-' )
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Partial Differential Equations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Types of PDEs}

  \begin{itemize}
  \item  Boundary-value problems:  elliptic PDEs
  \end{itemize}

  $$
\nabla ^2 u( x,y )
=
\frac{\partial^{2} u}{\partial x^{2}} + \frac{\partial^{2} u}{\partial y^{2}}
=
f( x,y )
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Types of PDEs}

  \begin{itemize}
  \item  Initial-value problems:  parabolic PDEs
  \end{itemize}

  $$
\frac{\partial u}{\partial t}
=
\alpha
\frac{\partial^{2} u}{\partial x^{2}}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Types of PDEs}

  \begin{itemize}
  \item  Initial-value problems:  hyperbolic PDEs
  \end{itemize}

  $$
\frac{\partial^{2} u}{\partial t^{2}}
=
c^{2}
\frac{\partial^{2} u}{\partial x^{2}}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Types of PDEs}

  $$
\frac{\partial^{2} u}{\partial x^{2}} + \frac{\partial^{2} u}{\partial y^{2}}
=
f( x,y )
  $$

  $$
\frac{\partial u}{\partial t}
=
\alpha
\frac{\partial^{2} u}{\partial x^{2}}
  $$

  $$
\frac{\partial^{2} u}{\partial t^{2}}
=
c^{2}
\frac{\partial^{2} u}{\partial x^{2}}
  $$
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{What Else is There?}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Other things to know}

  MATLAB concepts:
  \begin{itemize}
  \item  toolboxes:  Image Processing, Statistics, PDEs %\pause
  \item  parallel programming (\texttt{parfor}) %\pause
  \item  cells (like Python lists) %\pause
  \item  linear algebra, symbolic algebra
  \end{itemize}
\end{frame}

\end{document}
