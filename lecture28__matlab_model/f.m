function [ y ] = f( x )
y = cos( x ) - exp( -x ) + 4;
end %function
