for i = 1:100
    if mod( i,15 ) == 0
        fprintf( '%dfizzbuzz\n',i );
    elseif mod( i,3 ) == 0
        fprintf( '%dfizz\n',i );
    elseif mod( i,5 ) == 0
        fprintf( '%dbuzz\n',i );
    else
        fprintf( '%d\n',i );
    end %if
end %for