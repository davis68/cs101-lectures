%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Modeling in MATLAB}
\subtitle{CS 101}
\author{\texttt{Polynomials}}
\date{\texttt{lecture25} $\cdot$ \texttt{matlab/poly}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{matlab/poly} Objectives}

  \begin{enumerate}[label=\roman*]
  \item  Plot numerical values.
  \item  Create and evaluate polynomial arrays.
  \item  Numerically differentiate data using \texttt{polyder}.
  \item  Numerically integrate data using \texttt{polyint}.
  \item  Find function zeros and polynomial roots.
  \end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Plotting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Plotting}

  \begin{itemize}
  \item  \texttt{plot} works identically to \texttt{plt.plot}.
  \item  \texttt{figure} creates a new figure (window for plots).
  \end{itemize}
  \begin{Verbatim}
x = 0:.1:2*pi;
y = sin( x );
figure;
plot( x,y,'o' );
title( 'sin(x)' );
xlabel( 'x values' );
ylabel( 'y values' );
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Polynomials}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  $$
3 x^{3} + x^{2} - 4
  $$

  \begin{itemize}
  \item  MATLAB represents polynomials as row vectors of coefficients, from the highest-order term to the lowest.
  \end{itemize}
  \pause
  \begin{Verbatim}
  [ 3 1 0 -4 ]
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  $$
a_{1} x^{n} + a_{2} x^{n-1} + \cdot\cdot\cdot + a_{n-1} x^{2} + a_{n} x + a_{n+1}
  $$

A polynomial is a length $(n+1)$ array.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  \begin{tabular}{ll}
Polynomial & MATLAB Representation \\  \hline \\
$x + 1$    & \texttt{[ 1 1 ]}             \\
$x$        & \texttt{[ 1 0 ]}             \\
$x^2 + 1$  & \texttt{[ 1 0 1 ]}           \\
$x^2 + x + 1$ & \texttt{[ 1 1 1 ]}        \\
$x^4 + 2x^3 + 3x^2 + 4x + 5$  & \texttt{[ 1 2 3 4 5 ]} \\
$5x^4 + 4x^3 + 3x^2 + 2x + 1$ & \texttt{[ 5 4 3 2 1 ]} \\
$x^6$      & \texttt{[ 1 0 0 0 0 0 0 ]}   \\
  \end{tabular}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  $$
3 x^{3} + x^{2} - 4
  $$

  \begin{itemize}
  \item  To evaluate a polynomial for a particular value of $x$, use \texttt{polyval}:
  \end{itemize}
  \pause
  \begin{Verbatim}
polyval( [ 3 1 0 4 ],5 )  % for x = 5
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  \begin{itemize}
  \item  To plot a polynomial:
  \end{itemize}

  \begin{Verbatim}
p = [ -1 0 4 0 ]
x = -5:0.1:5
y = polyval( p,x )
plot( x,y,'r--' )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  \begin{itemize}
  \item  To multiply a polynomial, \texttt{*} won't work; use \texttt{conv} instead:
  \end{itemize}

  \begin{Verbatim}
u = [ 3 0 -1 ];
v = [ 2 5 ];
w = conv( u,v )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  \begin{itemize}
  \item  To differentiate a polynomial, use \texttt{polyder}:
  \end{itemize}
  $$
  p(x) = x^5 - x^4 + x^3 - x^2 + x - 1
  $$

  $$
  \frac{dp}{dx} = 5x^4 - 4x^3 + 3x^2 - 2x + 1
  $$

  \begin{Verbatim}
polynomial = [ 1 -1 1 -1 1 -1 ];
derivative = polyder( polynomial );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Polynomials}

  \begin{itemize}
  \item  To integrate a polynomial, use \texttt{polyint}:
  \end{itemize}
  $$
\int_{x=0}^{1} dx\, x^3 - x  \pause
=
\left.
\frac{x^4}{4} - \frac{x^2}{2}
\right|_{x=0}^{1}  \pause
=
\frac{1}{4} - \frac{1}{2} - \frac{0}{4} + \frac{0}{2}  \pause
=
-\frac{1}{4}
  $$
  \pause
  \begin{Verbatim}
integrand = [ 1 0 -1 0 ];
antiderivative = polyint( integrand );
integral_l = polyval( antiderivative,0 );
integral_u = polyval( antiderivative,1 );
integral = integral_u - integral_l;
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Aside:  Numerical Integration}

  \begin{itemize}
  \item  To integrate a set of points, use \texttt{trapz}:
  \end{itemize}
  \includegraphics[height=0.5\textwidth]{matlab_poly__polyi_numint0.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Aside:  Numerical Integration}

  \begin{itemize}
  \item  To integrate a set of points, use \texttt{trapz}:
  \end{itemize}
  \includegraphics[height=0.5\textwidth]{matlab_poly__polyi_numint1.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Aside:  Numerical Integration}

  \begin{itemize}
  \item  To integrate a set of points, use \texttt{trapz}:
  \end{itemize}
  \begin{Verbatim}
integrand = [ 1 0 -1 0 ];
antiderivative = polyint( integrand );
integral_l = polyval( antiderivative,0 );
integral_u = polyval( antiderivative,1 );
integral = integral_u - integral_l;
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Aside:  Numerical Integration}

  \begin{itemize}
  \item  To integrate a known function (not a polynomial), use \texttt{integral}:
  \end{itemize}
  \begin{Verbatim}
integral_value = integral( @sin,0,pi );
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Zeroes \& Roots}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Roots (Crossing of $x$ Axis)}

  $$
3 x^{3} + x^{2} - 4
  $$

  \begin{itemize}
  \item  To obtain the roots of the polynomial (as you would use when factoring it), use \texttt{roots}:
  \end{itemize}
  \pause
  \begin{Verbatim}
roots( [ 3 1 0 4 ] )
  \end{Verbatim}
  \begin{itemize}
  \item  How could you find out how many roots an arbitrary polynomial array has?
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Roots (Crossing of $x$ Axis)}

  $$
p(x) = x^5 - x^4 + x^3 - x^2 + x - 1
  $$

  $$
p(x) = ( x-1 )\left( x^2-x+1 \right)\left( x^2+x-1 \right)
  $$

  \begin{Verbatim}
p = [ 1 -1 1 -1 1 -1 ];
roots( p )
  \end{Verbatim}
  \pause
  \begin{itemize}
  \item  \texttt{poly} goes the other way:  a polynomial array from a set of roots.
  \end{itemize}
\end{frame}

\end{document}
