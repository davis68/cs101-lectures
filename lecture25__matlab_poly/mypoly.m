function [y] = mypoly(x)
  y = 3 * x .^ 3 + x .^ 2 - 4;
end

