function flag = isRightAngled(a, b, c)
x = a^2 + b^2
y = c^2
if x = y
flag = true
else
flag = false
end
