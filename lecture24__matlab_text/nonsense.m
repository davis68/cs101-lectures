function [ a b ] = nonsense( x,y )
    a = x .^ 2;
    b = y .^ 3;
end