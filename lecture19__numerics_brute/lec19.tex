%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Scientific Python}
\subtitle{CS 101}
\author{\texttt{Optimization I:  Brute Force}}
\date{\texttt{lecture19} $\cdot$ \texttt{numerics/brute}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{framenumber}{0}
\setcounter{showProgressBar}{1}
\setcounter{showSlideNumbers}{1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{numerics/brute} Objectives}

\begin{enumerate}[label=\roman*]
  \item  Apply a brute-force (comprehensive) search to solve problems relying on multiple dependent variables, with or without constraints.
  \item  Obtain permutations and combinations of items in a container using \texttt{itertools}.
  \item  Distinguish salient features of problems, such as size, convexity, and discreteness v. continuity.
  \item  Apply a library brute-force algorithm to optimize a 1D problem (\texttt{scipy.optimize.brute}).
\end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Optimization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  Brute-force search of a password:
  \end{itemize}
  \begin{Verbatim}
def check_password( pwd ):
    if pwd == 'pas':
        return True
    else:
        return False

chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
for pair in itertools.product( chars, repeat=3 ):
    pair = ''.join( pair )
    if check_password( pair ):
        print( pair )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  Brute-force search of a password:
  \end{itemize}
  $$
  \begin{array}{ll}
      & 2 \times n(\textrm{alphabet}) + n(\textrm{digits}) + n(\textrm{special}) \\
    = & 2 \times 26 + 10 + \{24\textrm{--}32\} \\
    = & \{86\textrm{--}94\}
  \end{array}
  $$
  \emph{per letter!}  This gets very big very quickly!
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  Given a function $f(\ubar{x})$, find $\ubar{x} = \ubar{x}^*$ such that $f(\ubar{x}^*)$ is maximized (or minimized).
  \item  The goal is to search the domain for the $\ubar{x}^*$ which yields the optimal $f(\ubar{x}^*)$.
  \item  Many clever techniques exist, but we'll start with a na\"{i}ve approach.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  On vacation, you purchase a collection of $n$ coins of varying weights and values.  When it comes time to pack, you find that your bag has a weight limit of 50 units.  What is the best set of items to take on the flight?
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% full-slide image
{ \setbeamertemplate{navigation symbols}{}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture,overlay]
    \node[at=(current page.center)] {
        \includegraphics[height=\paperheight]{./deadlift.png}
    };
  \end{tikzpicture}

  \begin{itemize}
  \item  Given a function $f(\ubar{x})$, find $\ubar{x} = \ubar{x}^*$ such that $f(\ubar{x}^*)$ is maximized (or minimized).
  \item  Brute-force searches the \emph{entire} domain of $f$.
  \item  How could we do this in our case?
  \end{itemize}
 \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  Two useful functions from the \texttt{itertools} module:
    \begin{itemize}
    \item  \texttt{combinations}:  provide all subsets of size \texttt{n}.
    \item  \texttt{permutations}:  provide all subsets of size \texttt{n}.
    \item  \texttt{product}:  replace nested \texttt{for} loops.
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  \texttt{combinations}:  provide all subsets of size \texttt{n}.
  \end{itemize}
  \begin{Verbatim}
from itertools import combinations

a = [ 1,2,3,4 ]
for x in combinations( a,2 ):
    print( x )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  \texttt{permutations}:  provide all orderings of subsets of size \texttt{n}.
  \end{itemize}
  \begin{Verbatim}
from itertools import permutations

a = [ 1,2,3,4 ]
for x in permutations( a,2 ):
    print( x )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Optimization}

  \begin{itemize}
  \item  \texttt{product}:  replace nested \texttt{for} loops.
  \item  Can use \texttt{repeat=n} argument as well.
  \end{itemize}
  \begin{Verbatim}
from itertools import product

a = [ 1,2,3,4 ]
b = [ 'g','h','i' ]
for x in product( a,b ):
    print( x )
for x in product( a, repeat=3 ):
    print( x )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% full-slide image
{ \setbeamertemplate{navigation symbols}{}
\begin{frame}[plain]
%
  \begin{tikzpicture}[remember picture,overlay]
    \node[at=(current page.center)] {
        \includegraphics[height=\paperheight]{./deadlift.png}
    };
  \end{tikzpicture}

  \begin{itemize}
  \item  Given a function $f(\ubar{x})$, find $\ubar{x} = \ubar{x}^*$ such that $f(\ubar{x}^*)$ is maximized (or minimized).  %\pause
  \item  Brute-force searches the \emph{entire} domain of $f$.
  \item  How could we do this in our case?
  \end{itemize}
 \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Setup}

  \begin{Verbatim}
import itertools

max_value = 0.0
max_set = None
for i in range(n):
    for set in itertools.combinations( items,i ):
        wts  = []
        vals = []
        for item in set:
            wts.append( weights[ item ] )
            vals.append( values[ item ] )
        value = f( wts,vals )
        if value > max_value:
            max_value = value
            max_set = set
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% full-slide image
{ \setbeamertemplate{navigation symbols}{}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture,overlay]
    \node[at=(current page.center)] {
        \includegraphics[height=\paperheight]{./deadlift.png}
    };
  \end{tikzpicture}

  \begin{itemize}
  \item  What if we need another constraint, like bulk volume?  \pause
  \item  Modify \texttt{f}, the figure of merit.
  \end{itemize}
 \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  \includegraphics[height=0.75\textheight]{numerics_brute__thrip_rastrigin1d.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Setup}

  \begin{Verbatim}
import numpy as np
def f( x ):
  return 10 + x ** 2 - 10 * np.cos( 2 * np.pi * x )

import matplotlib.pyplot as plt
x = np.linspace( -5,5,501 )
y = f( x )
plt.plot( x,y,'r-' )

from scipy.optimize import brute
result = brute( f,( ( -5,5 ), ),Ns=25 )
plt.plot( result,f(result),'ro' )
plt.show()
  \end{Verbatim}
\end{frame}


\end{document}
