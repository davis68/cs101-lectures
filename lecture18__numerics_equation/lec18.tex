%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Scientific Python}
\subtitle{CS 101}
\author{\texttt{Solving Equations}}
\date{\texttt{lecture18} $\cdot$ \texttt{numerics/equation}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{framenumber}{0}
\setcounter{showProgressBar}{1}
\setcounter{showSlideNumbers}{1}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{numerics/equation} Objectives}

\begin{enumerate}[label=\roman*]
  \item  Represent and solve equations in an efficient manner.
  \item  Use \texttt{\%timeit} to measure program run time.
  \item  Locate function zeroes using a graphical method or Newton's method.
  \item  Locate function minima using a graphical method or \texttt{scipy.optimize.minimize}.
\end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Equations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  \begin{itemize}
  \item  Suppose you wish to evaluate the function:
  \end{itemize}
  $$
  y = a \sin^3 x + b \sin^2 x + c \sin x + d
  $$  \pause
  \begin{itemize}
  \item  On a computer, which way is better?
  \end{itemize}
  \begin{Verbatim}
# Option 1
y = a*sin(x)**3 + b*sin(x)**2 + c*sin(x) + d

# Option 2
t = sin(x)
y = a*t**3 + b*t**2 + c*t + d
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  $$
  \exp(x)
  =
  1 + x + \frac{x^2}{2} + \frac{x^3}{6} + \frac{x^4}{24} + \cdots
  =
  \sum_{i=1}^{\infty} \frac{x^i}{i!}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  \begin{tabular}{lllll}
  Term Number & Expression & Value & Finite Approximate Value & Error \\
  \hline \\
  1     & $1$              & 1.0    & 1.0    & 6.3891 \\
  2     & $x$              & 2.0    & 3.0    & 4.3891 \\
  3     & $\frac{x^2}{2}$  & 2.0    & 5.0    & 2.3891 \\
  4     & $\frac{x^3}{6}$  & 1.3333 & 6.3333 & 1.0557 \\
  5     & $\frac{x^4}{24}$ & 0.6667 & 7.0    & 0.3891 \\
  6     & $\frac{x^5}{120}$ & 0.2667 & 7.2667 & 0.1224 \\
  7     & $\frac{x^6}{720}$ & 0.0889 & 7.3556 & 0.0335 \\
  8     & $\frac{x^7}{5040}$ & 0.02539 & 7.3810 & 0.0081 \\
  9     & $\frac{x^8}{40320}$ & 0.0063 & 7.3873 & 0.0018 \\
  $\infty$ & & & 7.38905… & 0 \\
  \end{tabular}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Timing Functions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  \includegraphics[height=0.75\textheight]{mc_pi.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  $$
\pi
=
4 \sum_{k=1}^{\infty}
\frac{\left( -1 \right)^{k+1}}{2k-1}
  $$
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Equations}

  \begin{itemize}
  \item  Which way is more efficient computationally?  \pause
  \item  The series solution is much better, and other better ways may exist.
  \item  We can quantify this if we can compare algorithms.
  \item  Generally, analytical solutions are faster than iterative solutions.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Code performance}

  \begin{itemize}
  \item  In order to compare algorithms, we need a way to measure code run time (called ``wallclock time'').
  \item  IPython provides a way to time your code: \texttt{\%timeit}
  \item  This runs your code many times and returns an average time to completion.
  \end{itemize}
  \begin{Verbatim}
%timeit  mc_pi( 3 )
%timeit  series_pi( 3 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fibonacci sequence}

  $$
  1\,\,\,1\,\,\,2\,\,\,3\,\,\,5\,\,\,8\,\,\,13\,\,\,21\,\,\,34\,\,\,55\,...
  $$
  \pause
  $$
  F_{n} = F_{n-1} + F_{n-2} \hspace{2cm} F_{1} = F_{2} = 1
  $$
  \pause
  $$
  F_{n} = \frac{\left( \frac{1+\sqrt{5}}{2} \right)^{n} + \left( \frac{2}{1+\sqrt{5}} \right)^{n}}{\sqrt{5}} + \frac{1}{2}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Analytical Fibonacci}

  \begin{Verbatim}
def fib_a( n ):
    sqrt_5 = 5**0.5;
    p = ( 1 + sqrt_5 ) / 2;
    q = 1 / p;
    return int( (p**n + q**n) / sqrt_5 + 0.5 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Recursive Fibonacci}

  \begin{Verbatim}
def fib_r( n ):
    if n == 1 or n == 2:
        return 1
    else:
        return fib_r( n-1 ) + fib_r( n-2 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Comparison}

  \begin{Verbatim}
%timeit fib_a( 12 )
%timeit fib_r( 12 )
  \end{Verbatim}
  \pause
  \begin{itemize}
  \item  On my machine, \texttt{fib\_a} is 55 $\times$ faster than \texttt{fib\_r} for \texttt{n = 12}.
  \item  Will this performance get better or worse for larger \texttt{n}?
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Comparison}

  \includegraphics[width=\textwidth]{numerics_equation__flick_timing.png}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  \begin{itemize}
  \item  If you find this sort of analysis interesting, I recommend:
    \begin{itemize}
    \item  CS 357, CS 450, TAM 470
    \item  Forman Acton's \emph{Numerical Methods that Work} (1970)
    \item  Abramowitz \& Stegun, \emph{Handbook of Mathematical Functions} (1964)
    \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solving Equations in $x$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  Let's consider how to find a specific solution to an equation, a value of $x$ for which $f(x)$ has a desired property.  \pause
  \begin{itemize}
  \item  Solutions for one or more unknowns
  \item  Extreme behavior
  \item  Net effect
  \item  Modeling
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Solving for $x$}

  \begin{itemize}
  \item  One quick-but-dirty way is to plot LHS v. RHS and find the crossover point:
  \end{itemize}
  \includegraphics[width=0.8\textwidth]{./lhs_rhs.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Solving for $x$}

  $$
\tan x + \exp x = x^{5}
  $$

  \begin{Verbatim}
tan(x) + exp(x) == x**5
  \end{Verbatim}
  \pause
  \includegraphics[width=0.75\textwidth]{./numerics_equation__mince_tan_zeros.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  \begin{itemize}
  \item  Newton's method uses the function and its derivative to locate the $x$ of the zero, $x^*$.
  \item  The trick, of course, is that you need the derivative.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  \begin{Verbatim}
def dfdx( f,x,h=1e-3 ):
    return ( f( x+h ) - f( x ) ) / h

def newton( f,x0,tol=1e-3 ):
    d = abs( 0 - f( x0 ) )
    while d > tol:
        x0 = x0 - f( x0 ) / dfdx( f,x0 )
        d = abs( 0 - f( x0 ) )
    return ( x0,f( x0 ) )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  $$
\cos x + 2 = x^3 - x^2
  $$

  \begin{Verbatim}
def eqn( x ):
    return ( np.cos( x ) + 2 ) - ( x**3 - x**2 )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  \begin{itemize}
  \item  The preceding code works okay, but a full implementation is available as \texttt{scipy.optimize.newton( f,x0 )}.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  \begin{itemize}
  \item  We can also find minima using \texttt{scipy.optimize.minimize(f,x0)}.  \pause
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  $$
f(x)
=
x^{2}+x-1
  $$

  \includegraphics[width=0.75\textwidth]{./numerics_equation__scrys_min.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Equations}

  \begin{tabular}{ll}
If you need to find this: & Use this:                 \\
\hline
minimum of a function     & \texttt{scipy.optimize.minimize} \\
zero of a function        & \texttt{scipy.optimize.newton}   \\
                          & graphical method          \\
solution to a function    & subtract and find zeroes  \\
  \end{tabular}
\end{frame}

\end{document}
