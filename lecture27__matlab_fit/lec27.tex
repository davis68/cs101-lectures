%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Modeling in MATLAB}
\subtitle{CS 101}
\author{\texttt{Curve Fitting}}
\date{\texttt{lecture27} $\cdot$ \texttt{matlab/fit}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{matlab/fit} Objectives}

  \begin{enumerate}[label=\roman*]
  \item  Fit polynomials to data, from linear to high-order.
  \item  Distinguish interpolation and fitting (regression).
  \item  Understand limitations of high-order fit.
  \item  Employ spline-based or piecewise fitting.
  \end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fitting Models to Data}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{itemize}
  \item  Given $n + 1$ points (or more), we can fit an $n$th order polynomial as a model using \texttt{polyfit}.
  \end{itemize}
  \begin{Verbatim}
x = rand( 3,1 );
y = rand( 3,1 );
xf = 0:0.01:1;
pf = polyfit( x,y,2 );
yf = polyval( pf,xf );
plot( x,y,'rx',  xf,yf,'r-' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{itemize}
  \item  Given $n + 1$ points (or more), we can fit an $n$th order polynomial as a model.
  \end{itemize}
  \begin{Verbatim}
x = rand( 5,1 );
y = rand( 5,1 );
xf = 0:0.01:1;
pf = polyfit( x,y,3 );
yf = polyval( pf,xf );
plot( x,y,'rx',  xf,yf,'r-' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{Verbatim}
n = input( 'Give an order' );
x = rand( n+1,1 );
y = rand( n+1,1 );
xf = 0:0.01:1;
pf = polyfit( x,y,n );
yf = polyval( pf,xf );
plot( x,y,'rx',  xf,yf,'r-' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{itemize}
  \item  High-order polynomials tend to wobble a lot, and overshoot at the ends.
  \item  A simpler fit is often better!
  \end{itemize}
  \begin{Verbatim}
x = rand( 11,1 );
y = rand( 11,1 );
xf = 0:0.01:1;
pf3  = polyfit( x,y,3 );
pf10 = polyfit( x,y,10 );
figure;
plot( x,y,'rx',xf,polyval( pf3,xf ),'r-',xf,polyval( pf10,xf ),'g-' );
ylim( [ -5 5 ] );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{itemize}
  \item  We would like to estimate the error of a fit.
  \item  Traditionally, this is the $L^2$ metric:
  $$
L^2 = | y_\text{known} - y_\text{fitted} |^2
  $$
  \item  This is always positive and should be close to zero.
  \end{itemize}
  \begin{Verbatim}
    x = rand( 11,1 );
    y = rand( 11,1 );
    xf = 0:0.01:1;
    pf  = polyfit( x,y,3 );
    err = sum( abs( y - polyval( pf,x ) ) .^ 2 );
    figure;
    ylim( [ 0 1 ] );
    plot( x,y,'rx',xf,polyval( pf3,xf ),'r-' );
    disp( err );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{itemize}
  \item  We would like to estimate the error of a fit.
  \item  Traditionally, this is the $L^2$ metric:
  $$
L^2 = | y_\text{known} - y_\text{fitted} |^2
  $$
  \item  This is always positive and should be close to zero.
  \item  (The quantity $y_\text{known}-y_\text{fitted}$ is the \emph{residual}.)
  \item  We may also want to know estimated error at any point (along the entire curve).
  \item  (This is easier if we have an analytical solution to compare to.)
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{Verbatim}
    x = rand( 11,1 );
    y = rand( 11,1 );
    xf = 0:0.01:1;
    pf  = polyfit( x,y,3 );
    resids = y - polyval( pf,x )
    figure;
    subplot( 2,1,1 );
    ylim( [ 0 1 ] );
    plot( x,y,'rx',xf,polyval( pf3,xf ),'r-' );
    subplot( 2,2,2 );
    plot( x,resids,'r.' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Fitting Polynomials}

  \begin{itemize}
  \item  We would like to estimate the error of a fit.
  \item  Traditionally, this is the $L^2$ metric:
  $$
L^2 = | y_\text{known} - y_\text{fitted} |^2
  $$
  \item  This is always positive and should be close to zero.
  \item  (The quantity $y_\text{known}-y_\text{fitted}$ is the \emph{residual}.)
  \item  We may also want to know estimated error at any point (along the entire curve).
  \item  (This is easier if we have an analytical solution to compare to.)
  \item  \textcolor{CS101Red}{A good fit has a small residual (but the absolute size is contextual).}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Linear interpolation}

  \begin{itemize}
  \item  A related process, \emph{interpolation} means estimating intermediate values (between two known points).
  \item  \textcolor{CS101Red}{Be careful to distinguish this from a linear fit (regression).}
  \end{itemize}

  \begin{tabular}{cc}
  \includegraphics[width=0.333\textwidth]{./interp1d-linear.png} & \includegraphics[width=0.333\textwidth]{./interp1d-spline.png}
  \end{tabular}
  \pause

  $$
y
=
y_0 + (x-x_0)\frac{y_1 - y_0}{x_1-x_0}
=
\frac{y_0(x_1-x)+y_1(x-x_0)}{x_1-x_0}
  $$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Linear interpolation 1/2}

  \begin{Verbatim}
x_samples = linspace( 0,1,11 );
y_samples = ( x_samples - 0.5 ) .^ 2 + 0.01 * randn( size( x_samples ) );

hold on
plot( x_samples,y_samples,'ro' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Linear interpolation 2/2}

  \begin{Verbatim}
x_span = linspace( 0,1,111 );
y_interp = interp1( x_samples,y_samples,x_span );
plot( x_span,y_interp,'b-' )

coefs = polyfit( x_samples,y_samples,2 );
y_regression = coefs(1)*x_span.^2 + coefs(2)*x_span + coefs(3);
plot( x_span,y_regression,'g-' )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Overfitting}

  \begin{itemize}
  \item  Insisting on too much precision can cause major problems!
  \item  Lower-order polynomial fits are best.
  \end{itemize}

  \includegraphics[width=0.333\textwidth]{./lec27-overfit.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Extrapolation}

  \begin{itemize}
  \item  Modeling outside of the domain of reliable data is fraught with difficulty.
  \item  Consider the behavior outside of the domain here for either curve.
  \item  Unless you have a very good theoretical reason, \textcolor{CS101Red}{\emph{don't}}!
  \end{itemize}

  \includegraphics[width=0.333\textwidth]{./lec27-overfit.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Spline interpolation}

  \begin{itemize}
  \item  Splines are piecewise polynomials designed to smoothly fit long segments without overfitting.
  \item  They are first-order continuous and thus differentiable.
  \end{itemize}

  \includegraphics[width=0.333\textwidth]{./splines.png}

  \begin{itemize}
  \item  Splines are typically constructed from cubic polynomials.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Brexit example}

  \begin{itemize}
  \item  Fit the Brexit data with the following methods:
    \begin{itemize}
    \item  Linear interpolation
    \item  Linear fit
    \item  Quadratic fit
    \item  Cubic fit
    \item  Quartic fit
    \item  Tenth-order fit
    \item  Spline fit
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{2D Interpolation}

  \begin{itemize}
  \item  \textcolor{CS101Midtone}{Not on \texttt{exam6}.}
  \item  In 2D, use \texttt{interp2} to get a single point, or \texttt{griddata} to reconstruct a whole grid:
  \end{itemize}

  \begin{Verbatim}
    x = 0:0.05:1;
    y = 0:0.05:1;
    z = rand( 21 );
    xstar = 0.33;
    ystar = 0.33;
    zstar = interp2( x,y,z,xstar,ystar );

    figure
    hold on
    mesh( x,y,z )
    plot3( xstar,ystar,zstar,'o' )
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{2D Interpolation}

  \begin{Verbatim}
    f = @( x,y ) x.*( 1-x ).*cos( 4*pi*x ) ...
                 .* sin( 2*pi*sqrt( y ) );

    % Define the basic grid coordinates.
    grid_x = 0:0.01:1;
    grid_y = grid_x( 1,: );

    % Define a random subset of the grid for which
    % we will generate data.
    pts = rand( 800,2 );  % x,y pairs
    vals = f( pts( :,1 ),pts( :,2) );

    % Generate a grid.  'nearest','linear','cubic'
    [ X Y ] = meshgrid( grid_x,grid_y );
    Z = griddata( pts( :,1 ),pts( :,2 ),vals, ...
                  X,Y,'linear' );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{2D Interpolation}

  \begin{Verbatim}
    hold on
    mesh( X,Y,Z )
    plot3( pts( :,1 ),pts( :,2 ),f(pts(:,1),pts(:,2)),'r.' )
  \end{Verbatim}
\end{frame}

\end{document}
