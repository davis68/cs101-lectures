    f = @( x,y ) x.*( 1-x ).*cos( 4*pi*x ) .* sin( 2*pi*sqrt( y ) );

    % Define the basic grid coordinates.
    grid_x = 0:0.01:1;
    grid_y = grid_x( 1,: );

    % Define a random subset of the grid for which
    % we will generate data.
    pts = rand( 800,2 );  % x,y pairs
    vals = f( pts( :,1 ),pts( :,2) );

    % Generate a grid.  'nearest','linear','cubic'
    [ X Y ] = meshgrid( grid_x,grid_y );
    Z = griddata( pts( :,1 ),pts( :,2 ),vals,X,Y,'nearest' );

    hold on;
    mesh( X,Y,Z )
    plot3( pts( :,1 ),pts( :,2 ),f(pts(:,1),pts(:,2)),'r.' )
