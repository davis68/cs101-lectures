% This recapitulates the data we pulled last time.
% Let's just examine one data set, column 2, 'Remain'
poll = importdata( 'brexit.csv' );
figure
legend( gca,'show' )
hold on
plot( poll.data( :,2 ),'ko','DisplayName','Raw Data' );

n = numel( poll.data( :,2 ) );
x = linspace( 1,n,n );
y = poll.data( :,2 )';
x_fit = linspace( 1,n,2001 );

% Linear interpolation
y_interp1 = interp1( x,y,x_fit );
plot( x_fit,y_interp1,'m--','DisplayName','Linear interpolation' );

% Linear fit
p_linear = polyfit( x,y,1 );
y_linear = polyval( p_linear,x_fit );
plot( x_fit,y_linear,'r--','DisplayName','Linear fit' );

% Quadratic fit
p_quad = polyfit( x,y,2 );
y_quad = polyval( p_quad,x_fit );
plot( x_fit,y_quad,'g--','DisplayName','Quadratic fit' );

% Cubic fit
p_cubic = polyfit( x,y,3 );
y_cubic = polyval( p_cubic,x_fit );
plot( x_fit,y_cubic,'k--','DisplayName','Cubic fit' );

% Quartic fit
p_quar = polyfit( x,y,4 );
y_quar = polyval( p_quar,x_fit );
plot( x_fit,y_quar,'g.','DisplayName','Quartic fit' );

% Spline interpolation
y_spline= spline( x,y,x_fit );
plot( x_fit,y_spline,'c-','DisplayName','Spline interpolation' );

