%!TEX program = xelatex
\documentclass[11pt]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage{fancyvrb}
\usepackage{booktabs}
\usepackage{ragged2e}  % for \justifying

\newcommand{\ubar}[1]{\text{\underline{$#1$}}}

\hypersetup{pdfborder = {0 0 0}}

\usetheme{Blue}

\title{Modeling in MATLAB}
\subtitle{CS 101}
\author{\texttt{Introduction}}
\date{\texttt{lecture23} $\cdot$ \texttt{matlab/intro}}

\setcounter{showSlideNumbers}{1}

\newcommand{\correctstar}{\textcolor{CS101Red}{$\bigstar$}}

\newcounter{QuestionCounter}
\setcounter{QuestionCounter}{0}

\begin{document}
  \setcounter{showProgressBar}{0}
  \setcounter{showSlideNumbers}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\titlepage}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{\texttt{matlab/intro} Objectives}

  \begin{enumerate}[label=\roman*]
  \item  Explore the MATLAB user interface.
  \item  Index and slice arrays.
  \item  Compose basic functions.
  \item  Distinguish vector and matrix operations.
  \item  Create basic loops (\texttt{for}/\texttt{while}).
  \item  Employ conditional logic (\texttt{if}/\texttt{else}/\texttt{end} statements).
  \item  Distinguish MATLAB boolean values.
  \item  Utilize MATLAB-specific data types like \texttt{datetime}.
  \end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% full-slide image
{ \setbeamertemplate{navigation symbols}{}
    \begin{frame}[plain]
        \begin{tikzpicture}[remember picture,overlay]
            \node[at=(current page.center)] {
                \includegraphics[width=\paperwidth]{./img/matlab-disk.jpg}
            };
        \end{tikzpicture}
        %https://twitter.com/andrenarchy/status/560385707244789760
     \end{frame}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Interface}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Interface}

  \begin{itemize}
  \item  Start MATLAB either at the command line, \texttt{matlab}, or by clicking the icon.
  \end{itemize}
  \begin{centering}
  \includegraphics[width=0.5\paperwidth]{./img/matlab-layout.jpg}
  \end{centering}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Why MATLAB?}

  \begin{itemize}
  \item  Designed for engineering.
  \item  Excellent documentation and toolboxes.
  \item  Strong areas of application:
    \begin{itemize}
    \item  Linear algebra
    \item  Control dynamics
    \item  Numerical analysis
    \item  Image processing
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Why MATLAB?}

  \begin{itemize}
  \item  Can you do anything with it that you can't do in Python?  \pause
  \item  All programming languages can be made ``equivalent''---so it depends on the libraries and applications, and the culture of your working group.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{What is MATLAB?}

  \begin{itemize}
  \item  Programming language + environment.
  \item  Proprietary, owned and maintained by MathWorks.
  \item  Dates from late 1970s, under active development.
  \item  Was an influence on NumPy/MPL, so will have familiar syntax.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Basics}

  \begin{itemize}
  \item  Literals, variables, operators
  \end{itemize}
  \begin{Verbatim}
4 ^ 3
  \end{Verbatim}
  \begin{itemize}
  \item  Expressions
  \end{itemize}
  \begin{Verbatim}
a = 3 * 2
b = 1 + a
  \end{Verbatim}
  \begin{itemize}
  \item  Semicolon suppresses output (mutes):  \texttt{;}
  \end{itemize}
  \begin{Verbatim}
b = b + 2;
  \end{Verbatim}
  \begin{itemize}
  \item  \texttt{ans} is default result.
  \end{itemize}
  \begin{Verbatim}
a / 4
  \end{Verbatim}
  \begin{itemize}
  \item  \texttt{fprintf} displays the value only.
  \end{itemize}
  \begin{Verbatim}
fprintf( ans );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Numeric types}

  \begin{itemize}
  \item  MATLAB implements:
    \begin{itemize}
    \item  integers
    \item  floating-point numbers
    \item  complex numbers
    \end{itemize}
  \item  in 8-, 16-, 32-, and 64-bit versions (like NumPy).
  \item  \texttt{whos} shows type, value of all variables in workspace.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Array types}

  \begin{itemize}
  \item  Arrays are the fundamental type in MATLAB:
  \end{itemize}
  \begin{Verbatim}
a = [ 1 2 3 ];
  \end{Verbatim}
  \begin{itemize}
  \item  Arrays are indexed using parentheses:
  \end{itemize}
  \begin{Verbatim}
b = a( 1 );
  \end{Verbatim}
  \pause
  \begin{itemize}
  \item  \textcolor{red}{MATLAB indexes from one, not zero!}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Multidimensional arrays}

  \begin{itemize}
  \item  More dimensional arrays use semicolons to separate rows:
  \end{itemize}
  \begin{Verbatim}
A = [ 1 2 3 ; 4 5 6 ];
  \end{Verbatim}
  \begin{itemize}
  \item  Arrays are indexed using parentheses and commas:
  \end{itemize}
  \begin{Verbatim}
a = A( 1,2 );
  \end{Verbatim}
  \begin{itemize}
  \item  Helper functions are available:
  \end{itemize}
  \begin{Verbatim}
B = ones( 3,3 ) + eye( 3,3 ) + zeros( 3,3 );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{QuestionCounter}
\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
\left(
\begin{array}{ccc}
1 & 1 & 1 \\
2 & 2 & 2
\end{array}
\right)
$$

Which of the following will produce this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{[ 1 1 1 ] ; [ 2 2 2 ]}
    \item  \texttt{[ 1 1 1 ; 2 2 2 ]}
    \item  \texttt{[ 1 2 ] ; [ 1 2 ] ; [ 1 2 ]}
    \item  \texttt{[ 1 2 ; 1 2 ; 1 2 ]}
    \item  \texttt{[ [ 1 1 1 ] , [ 2 2 2 ] ]}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
\left(
\begin{array}{ccc}
1 & 1 & 1 \\
2 & 2 & 2
\end{array}
\right)
$$

Which of the following will produce this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{[ 1 1 1 ] ; [ 2 2 2 ]}
    \item  \texttt{[ 1 1 1 ; 2 2 2 ]} \correctstar
    \item  \texttt{[ 1 2 ] ; [ 1 2 ] ; [ 1 2 ]}
    \item  \texttt{[ 1 2 ; 1 2 ; 1 2 ]}
    \item  \texttt{[ [ 1 1 1 ] , [ 2 2 2 ] ]}
  \end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{QuestionCounter}
\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
A =
\left(
\begin{array}{ccc}
1 & 2 & 3 \\
4 & 5 & 6
\end{array}
\right)
$$

Which of the following will access \texttt{4} in this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{A( 1,0 )}
    \item  \texttt{A[ 2,1 ]}
    \item  \texttt{A( 2,1 )}
    \item  \texttt{A( 1 )( 0 )}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
A =
\left(
\begin{array}{ccc}
1 & 2 & 3 \\
4 & 5 & 6
\end{array}
\right)
$$

Which of the following will access \texttt{4} in this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{A( 1,0 )}
    \item  \texttt{A[ 2,1 ]}
    \item  \texttt{A( 2,1 )} \correctstar
    \item  \texttt{A( 1 )( 0 )}
  \end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Array operations}

  \begin{Verbatim}
% basic mathematics:
A = ( ones( 3,3 ) + 1 ) / 2
B = sin( ones( 3,3 ) * pi )
C = B'  % transpose with '

% matrix multiplication:
D = eye( 3,4 ) * ones( 4,5 ) * pi
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{QuestionCounter}
\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
\left(
\begin{array}{cc}
2 & 1 \\
1 & 2
\end{array}
\right)
$$

Which of the following will produce this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{3*ones( 2,2 ) - 2*eye( 2,2 )}
    \item  \texttt{2*ones( 2,2 ) + eye( 2,2 )}
    \item  \texttt{3*ones( 2,2 ) - eye( 2,2 )}
    \item  \texttt{ones( 2,2 ) + eye( 2,2 )}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
\left(
\begin{array}{cc}
2 & 1 \\
1 & 2
\end{array}
\right)
$$

Which of the following will produce this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{3*ones( 2,2 ) - 2*eye( 2,2 )}
    \item  \texttt{2*ones( 2,2 ) + eye( 2,2 )}
    \item  \texttt{3*ones( 2,2 ) - eye( 2,2 )}
    \item  \texttt{ones( 2,2 ) + eye( 2,2 )}  \correctstar
  \end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Array operations}

  \begin{Verbatim}
% concatenating arrays
A = [  eye( 3,4 ),  eye( 3,5 );
      ones( 2,4 ), ones( 2, 5) ]
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{QuestionCounter}
\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
\left(
\begin{array}{cc}
1 & 2 \\
3 & 4 \\
5 & 6
\end{array}
\right)
$$

How can we produce this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{[ [ 1 3 5 ] [ 2 4 6 ] ]}
    \item  \texttt{[ [ 1 2 ] [ 3 4 ] [ 5 6 ] ]}
    \item  \texttt{[ [ 1 3 5 ] ; [ 2 4 6 ] ]}
    \item  \texttt{[ [ 1 2 ] ; [ 3 4 ] ; [ 5 6 ] ]}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Question~\theQuestionCounter}

  $$
\left(
\begin{array}{cc}
1 & 2 \\
3 & 4 \\
5 & 6
\end{array}
\right)
$$

How can we produce this array?

  \begin{enumerate}[label=\Alph*]
    \item  \texttt{[ [ 1 3 5 ] [ 2 4 6 ] ]}
    \item  \texttt{[ [ 1 2 ] [ 3 4 ] [ 5 6 ] ]}
    \item  \texttt{[ [ 1 3 5 ] ; [ 2 4 6 ] ]}
    \item  \texttt{[ [ 1 2 ] ; [ 3 4 ] ; [ 5 6 ] ]} \correctstar
  \end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Scripting}

  \begin{itemize}
  \item  MATLAB uses \texttt{.m} files for two purposes:  scripts and functions.
  \item  Comments are indicated as follows:
  \end{itemize}
  \begin{Verbatim}
% this is a comment
%{
  this is a block comment
%}
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Scripting}

  \begin{itemize}
  \item  Use the built-in editor to create these.
  \item  Make sure you have the correct working directory.
  \item  Scripts contain regular commands in order of execution.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Functions}

  \begin{itemize}
  \item  Functions must be located in a file of the same name as the function.
  \end{itemize}
  \begin{Verbatim}
function [ output ] = function_name( input )
    % ...
end
  \end{Verbatim}
  \begin{itemize}
  \item  No explicit \texttt{return} statements---rely on values in output variable list.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Functions}

$$
T_{\text{F}} = \frac{180}{100} T_{\text{C}} + 32
$$

File \texttt{TempC2F.m}:
  \begin{Verbatim}
function [ Tf ] = TempC2F( Tc )
    Tf = Tc * ( 180/100 ) + 32;
end
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Strings}

  \begin{itemize}
  \item  Indicated with single quotes (only!).
  \end{itemize}
  \begin{Verbatim}
s = 'XFEM';
  \end{Verbatim}
  \begin{itemize}
  \item  Print formatted strings with \texttt{fprintf}:
  \end{itemize}
  \begin{Verbatim}
fprintf( '%f %f', sin(pi/3), cos(pi/4) );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Matrix v. element operations}

  \begin{itemize}
  \item  ``Matrix dimensions must agree.''
  \item  It is necessary to distinguish \emph{elementwise} operations and \emph{matrix} operations. %\pause
  \end{itemize}
  \begin{Verbatim}
A = 2 * ones( 2,2 )
B = A .* eye( 2,2 )
C = A  * eye( 2,2 )
  \end{Verbatim}
  \begin{itemize}
  \item  These are distinguished by a dot \texttt{.} in front of the operator.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{for} statement}

  \begin{Verbatim}
%% loop through time steps
for i = 1:2:10
    fprintf( 'The number is %i.' , i );
end
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{for} statement}

  \begin{itemize}
  \item  The \texttt{for} loop ranges over a set of possible values. \pause
  \item  This is \emph{not} as flexible as Python's \texttt{in} syntax---think of always having to loop over the \emph{index} rather than the item.
  \item  Ranges are straightforward:  \texttt{1:10}, \texttt{1:2:10}, \texttt{0.1:0.1:0.5}.  Also have \texttt{linspace} available.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{for} statement}

  \begin{itemize}
  \item  We create a \texttt{for} loop as follows:
    \begin{itemize}
    \item  statement \texttt{for var = range}, where you create \texttt{var} and provide \texttt{range}
    \item  one or more statements
    \item  closing statement \texttt{end}
    \end{itemize}
  \item  Also have \texttt{continue} and \texttt{break} available.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Example:  \texttt{absolute.m}}

  \begin{semiverbatim}
function [ y ] = absolute( x )
    y = 0;
    if x >= 0
        y = x;
    else
        y = -x;
end %function
  \end{semiverbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{if}/\texttt{else} statement}

  \begin{itemize}
  \item  We create an \texttt{if}/\text{else} statement as follows:
    \begin{itemize}
    \item  the keyword \texttt{if}
    \item  a logical comparison \textcolor{red}{(more on these!)}
    \item  a \textbf{block} of code  \pause
    \item  the keyword \texttt{elseif} \textcolor{red}{(note this!)}
    \item  a new logical comparison
    \item  a different \textbf{block} of code  \pause
    \item  the keyword \texttt{else}
    \item  a different \textbf{block} of code  \pause
    \item  the keyword \texttt{end}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Logical statements}

  \begin{itemize}
  \item  MATLAB does \emph{not} have a \texttt{bool} data type. \pause
  \item  Instead of \texttt{True}/\texttt{False}, MATLAB uses integers:
    \begin{itemize}
    \item  \texttt{0} means \texttt{False}
    \item  \texttt{1} means \texttt{True}
    \end{itemize}
  \pause
  \item  Available logical operators include:
    \begin{itemize}
    \item  \texttt{<}, \texttt{>}, \texttt{<=}, \texttt{>=}, \texttt{==}, \texttt{\textasciitilde =}
    \item  \texttt{\&\&} for `and', \texttt{||} for `or'
    \item  \texttt{ismember} checks equality of elements in arrays.
    \item  Also, logical operators work as indices! \pause
    \item  \texttt{A( A<0 )}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{\texttt{while} loop}

  \begin{Verbatim}
%% loop until condition is met
i = 0;
while i < 10
    i = i + 1;
    fprintf( 'The number is %i.' , i );
end
  \end{Verbatim}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://www.mathworks.com/help/matlab/ref/datetime.html
\begin{frame}[fragile]
  \frametitle{\texttt{datetime} type}

  \begin{itemize}
  \item  Dates and times can usefully be stored as values:
  \end{itemize}
  \begin{Verbatim}
t = datetime( Y,M,D,H,MI,S );
t = datetime( 'now','TimeZone','local','Format','d-MMM-y HH:mm:ss Z' );
t = datetime( '2017-12-01','InputFormat','yyyy-MM-dd' );
fprintf( t );
  \end{Verbatim}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Summary}

  \begin{itemize}
  \item  Like NumPy, but no \texttt{import}s (anywhere).
  \item  Remember to change:  parentheses, indexing from 1, \texttt{end} keywords.
  \item  Hard to do \texttt{dict}-like things, easy to do \texttt{numpy}-like operations.
  \end{itemize}
\end{frame}

\end{document}
