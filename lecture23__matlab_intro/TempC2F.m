function [ Tf ] = TempC2F( Tc)
  Tf = Tc * ( 180/100 ) + 32;
end %function