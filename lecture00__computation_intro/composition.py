import pandas as pd

df = pd.read_csv('rpt_all_students.csv')

import matplotlib.pyplot as plt
from palettable.cubehelix import cubehelix1_16 as ch

#https://github.com/jiffyclub/palettable/blob/master/palettable/cubehelix/cubehelix.py
from palettable.cubehelix import Cubehelix
cm = Cubehelix.make(gamma=1.0, start=1.5, rotation=-1.0, sat=1.5, n=len(pd.unique(df['Major 1 Name']))+2).mpl_colors

majors = sorted(pd.unique(df['Major 1 Name']),reverse=True)
counts = []
for major in majors:
    counts.append(df[df['Major 1 Name']==major].shape[0])

fig,ax = plt.subplots()
ax.barh(range(len(counts)), counts, tick_label=majors, height=1.0, color=cm[:-2], edgecolor='black', linewidth=1)
for i,v in enumerate(counts):
    ax.text(v+1,i-0.25,str(v))

plt.title('Student Composition by Major', fontdict={'family':'Inter', 'weight':'black', 'fontsize': 24})
plt.tight_layout()
plt.show()

#########################

from palettable.cubehelix import Cubehelix
cm = Cubehelix.make(gamma=1.0, start=1.5, rotation=-1.0, sat=1.5, n=len(pd.unique(df['College']))+2).mpl_colors

majors = sorted(pd.unique(df['College']),reverse=True)
counts = []
for major in majors:
    counts.append(df[df['College']==major].shape[0])

fig,ax = plt.subplots()
ax.barh(range(len(counts)), counts, tick_label=majors, height=1.0, color=cm[:-2], edgecolor='black', linewidth=1)
for i,v in enumerate(counts):
    ax.text(v+1,i+0.0,str(v))

plt.title('Student Composition by College', fontdict={'family':'Inter', 'weight':'black', 'fontsize': 24})
plt.tight_layout()
plt.show()

#########################

# Star Chart

#https://github.com/jiffyclub/palettable/blob/master/palettable/cubehelix/cubehelix.py
from palettable.cubehelix import Cubehelix
cm = Cubehelix.make(gamma=1.0, start=1.5, rotation=-1.0, sat=1.5, n=len(pd.unique(df['Major 1 Name']))+3).mpl_colors

majors = sorted(pd.unique(df['Major 1 Name']),reverse=True)
counts = []
for major in majors:
    counts.append(df[df['Major 1 Name']==major].shape[0])

xrun = 720 // 24
yrun = 720 // xrun

fig,ax = plt.subplots()
from itertools import product
for index,major in enumerate(df['Major 1 Name']):
    x = index % xrun
    y = index // xrun
    ax.plot(x,y,'*',color=cm[:-3][majors.index(major)],markersize=12)

plt.title('Student Composition by Major', fontdict={'family':'Inter', 'weight':'black', 'fontsize': 24})
plt.tight_layout()
plt.show()


#########################

# Star Chart

#https://github.com/jiffyclub/palettable/blob/master/palettable/cubehelix/cubehelix.py
from palettable.cubehelix import Cubehelix
cm = Cubehelix.make(gamma=1.0, start=1.5, rotation=-1.0, sat=1.5, n=len(pd.unique(df['College']))+2).mpl_colors

majors = sorted(pd.unique(df['College']),reverse=True)
counts = []
for major in majors:
    counts.append(df[df['College']==major].shape[0])

xrun = 720 // 24
yrun = 720 // xrun

fig,ax = plt.subplots()
from itertools import product
for index,major in enumerate(df['College']):
    x = index % xrun
    y = index // xrun
    ax.plot(x,y,'*',color=cm[:-2][majors.index(major)],markersize=12)

plt.title('Student Composition by College', fontdict={'family':'Inter', 'weight':'black', 'fontsize': 24})
plt.tight_layout()
plt.show()
