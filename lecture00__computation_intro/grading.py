import matplotlib.pyplot as plt
#from palettable.cubehelix import cubehelix3_16 as ch
from palettable import cubehelix
ch = cubehelix.Cubehelix.make(start=2.75, rotation=0., n=16)  #blue
ch = cubehelix.Cubehelix.make(start=1.666, rotation=0., n=16)  #green
ch = cubehelix.Cubehelix.make(start=1., rotation=0., n=16)  #red
ch = cubehelix.Cubehelix.make(start=1.333, rotation=0., n=16)  #yellow
'''
from matplotlib.font_manager import _rebuild; _rebuild()
'''

lessons = 1
n_lessons = 25
c_lessons = ch.hex_colors[14]
labs = 2.5
n_labs = 10
c_labs = ch.hex_colors[11]
hws = 2.5
n_hws = 10
c_hws = ch.hex_colors[8]
quizzes = 5
n_quizzes = 5
c_quizzes = ch.hex_colors[5]

sizes = []
#sizes.extend([lectures]*n_lecs)
sizes.extend([lessons]*n_lessons)
sizes.extend([labs]*n_labs)
sizes.extend([hws]*n_hws)
sizes.extend([quizzes]*n_quizzes)
colors = []
#colors.extend([c_lec]*n_lecs)
colors.extend([c_lessons]*n_lessons)
colors.extend([c_labs]*n_labs)
colors.extend([c_hws]*n_hws)
colors.extend([c_quizzes]*n_quizzes)
labels = []
#labels.extend([f'lesson'+"{0}".format(n).rjust(2) for n in range(n_lecs)])
#labels.extend([f'lab'+"{0}".format(n).rjust(2) for n in range(n_labs)])
#labels.extend([f'hw'+"{0}".format(n).rjust(2) for n in range(n_hws)])
#labels.extend([f'quiz{n}' for n in range(n_quizzes)])
#labels.extend(['']*(n_lecs//2-1)+['lectures\n(1% each)']+['']*(n_lecs//2))
labels.extend(['']*(n_lessons//2)+['lessons\n(1% each)']+['']*(n_lessons//2))
labels.extend(['']*(n_labs//2-1)+['labs\n(2½% each)']+['']*(n_labs//2))
labels.extend(['']*(n_hws//2-1)+['homeworks\n(2½% each)']+['']*(n_hws//2))
labels.extend(['']*(n_quizzes//2)+['quizzes\n(5% each)']+['']*(n_quizzes//2))
patches, texts = plt.pie(sizes, colors=colors, startangle=90, labels=labels, textprops={'family': 'Inter', 'weight':'light', 'fontsize': 24}, wedgeprops={'edgecolor': 'white','linewidth': 0.2})
#plt.legend(patches, labels, loc="best")
plt.axis('equal')
plt.title('Grading Breakdown', fontdict={'family':'Ubuntu', 'weight':'black', 'fontsize': 36})
#plt.tight_layout()
plt.show()
