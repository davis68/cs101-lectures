import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

width = 8
height = 4

fig,axes = plt.subplots(nrows=height,ncols=width)
entries = [
  'x','computation/\nintro','python/\nintro','python/\ntypes','python/\nlogic',
  'python/\nflow','python/\nloops','python/\nfiles','python/\ndict',
  'python/\nmemory','python/\nerror','symbolics/\nalgebra',
  'symbolics/\ncalculus','symbolics/\nphysics','numerics/\nplotting',
  'numerics/\nvector','x','x','numerics/\nrandom','numerics/\nmatrix',
  'numerics/\ncalculus','numerics/\nequation','numerics/\nbrute',
  'numerics/\ngreedy','numerics/\nmonte','python/\nmodule','matlab/\nintro',
  'matlab/\ntext','matlab/\npoly','matlab/\nstats','matlab/\nfit','matlab/\nmodel'
]

for index,name in enumerate(entries):
    i,j = index%height,(index//height)%width
    ax = axes[i,j]
    patch = mpatches.Ellipse((0.5,0.5),0.8,0.4,facecolor='pink',edgecolor='red',linewidth=2)
    ax.add_patch(patch)
    ax.text(0.5,0.5,name,ha="center",va="center",color="w",fontdict={'family': 'Inter', 'weight':'bold', 'fontsize': 24})
    ax.axis('equal')
    ax.axis('off')
#plt.tight_layout()
plt.show()

# TODO:  fix scaling, spacing, add Xs for gaps
